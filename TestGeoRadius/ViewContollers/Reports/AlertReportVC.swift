//
//  AlertReportVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 14/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class AlertReportVC: UIViewController {

    @IBOutlet weak var btn_filter: UIButton!
    @IBOutlet weak var view_flash: UIView!
    @IBOutlet weak var view_date_picker_perent: UIView!
    @IBOutlet weak var view_alerts_reports: UIView!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var date_and_time_picker: UIDatePicker!
    @IBOutlet weak var header_view: UIView!
    @IBOutlet weak var txt_select_vehicle: UITextField!
    @IBOutlet weak var txt_alert_type: UITextField!
    @IBOutlet weak var txt_to_date: UITextField!
    @IBOutlet weak var txt_from_date: UITextField!
    @IBOutlet weak var tbl_shot_item: UITableView!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var view_table: UIView!
    let alert_view = AlertView.instanceFromNib()
    var report_data_controller : AlertReportSubmitVC?
    var indexPath_val = [Int]()
    var indexPath_val_remove = [Int]()
    
    var indexPath_val_alert = [Int]()
    var indexPath_val_remove_alert = [Int]()
    
    var alert_type_values = [String]()
    var vehicle_type_values = [String]()
    
    var alert_type_id = [String]()
    var device_id = [String]()
    
    var alert_report_origin : CGFloat!
    var view_height : CGFloat!
    var button_origin_y : CGFloat!
    var vehicle_data = [String]()
    var alert_data = [String]()
    var alert_type = [String]()
     var filterdData : [String]!
    var isVehicleType = true
    var isFromDate = true
    var selected_values = [String]()
    var remove_values = [String]()
    @IBOutlet weak var view_todate_fromdate: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        header_view.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        self.tbl_shot_item.register(UINib(nibName: "VehicleCell", bundle: nil), forCellReuseIdentifier: "cell_vehicle")
        date_and_time_picker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        btn_submit.addTarget(self, action: #selector(submit_pressed), for: .touchUpInside)
      // self.tbl_shot_item.allowsMultipleSelection = true
        self.view_todate_fromdate.isHidden = true
        alert_report_origin = view_alerts_reports.frame.origin.y
        view_alerts_reports.layer.cornerRadius = view_alerts_reports.frame.size.height / 29
//        view_alerts_reports.layer.borderColor = UIColor.lightGray.cgColor
//        view_alerts_reports.layer.borderWidth = 1
//        view_alerts_reports.layer.masksToBounds = true
        SetReportView(viewName: view_alerts_reports)
        txt_to_date.resignFirstResponder()
        view_flash.backgroundColor = UIColor.clear
//        let date = Date()
//        let formetter = DateFormatter()
//        formetter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        let result = formetter.string(from: date)
        txt_to_date.text = TodayToDate()
//        let full_date_time = result.components(separatedBy: " ")
//        let to_date = full_date_time[0]
//        let fromdate = to_date + " 00:00:00"
        txt_from_date.text = TodayFromDate()
        SetTextFieldLeftSide(imageName: "search", txt_field: txt_search)
        btn_submit.layer.cornerRadius = btn_submit.frame.size.height / 4
      SetOrigin()
        // Do any additional setup after loading the view.
    }
    
  
    
    
    @objc func submit_pressed(){
        
        if txt_to_date.text! <  txt_from_date.text!{
            showToast(controller: self, message: "End Date should be greater then Start Date.", seconds: 1.5)
            return
        }
        
        if txt_to_date.text! ==  txt_from_date.text!{
            showToast(controller: self, message: "End Date should be greater then Start Date.", seconds: 1.5)
            return
        }
        
        if txt_from_date.text!.count < 1 || txt_to_date.text!.count < 1{
            showToast(controller: self, message: "Fields can't be Empty", seconds: 0.3)
            return
        }
        
        var alert_val = [String]()
        var vehicle_val = [String]()
        
        for index in indexPath_val_alert{
            if index != 0{
                alert_val.append(alert_type_id[index])
            }
        }
        
        for index in indexPath_val{
            vehicle_val.append(device_id[index])
            
        }
        
        
        if alert_val.count < 1 || vehicle_val.count < 1{
            showToast(controller: self, message: "Fields can't be Empty", seconds: 0.5)
            return
        }
        
//       let to_date_time = txt_to_date.text
//        let full_date_time = to_date_time?.components(separatedBy: " ")
        
        let to_date = GetToDate(date : txt_to_date.text!)
        let to_time = GetToTime(time : txt_to_date.text!)
        
//        let from_date_time = txt_from_date.text
//        let date_time = from_date_time?.components(separatedBy: " ")
        
        let from_date = GetFromDate(date: txt_from_date.text!)
        let from_time = GetFromTime(time: txt_from_date.text!)
       
      
//        if !view_todate_fromdate.isHidden{
//            UIView.animate(withDuration: 0.5, animations: {
//                self.SetOrigin()
//            })
//            view_todate_fromdate.isHidden = true
//        }
          btn_filter.setImage(UIImage(named: "filterdown"), for: .normal)
        UIView.animate(withDuration: 0.5, animations: {
            self.view_alerts_reports.frame.origin.y = self.header_view.frame.size.height + 30
            //self.view_alerts_reports.isHidden = false
        })
        view_alerts_reports.isHidden = false
        guard let locationController = children.first as? AlertReportSubmitVC else  {
            fatalError("Check storyboard for missing LocationTableViewController")
        }
        report_data_controller = locationController
        
        report_data_controller?.SetValues(device_id : vehicle_val, alert_type_id: alert_val, to_date: to_date, to_time: to_time, from_date: from_date, from_time: from_time)
        
    }
    
    @IBAction func pressed_date_done(_ sender: Any) {
         view_date_picker_perent.isHidden = true
    }
    
    func SetOrigin(){
        
        view_height = view_todate_fromdate.frame.size.height
        
        self.view_todate_fromdate.frame.size.height = 0
        
        button_origin_y = btn_submit.frame.origin.y
        
        btn_submit.frame.origin.y =  view_todate_fromdate.frame.origin.y + 30.0
        
    
    }
    
    @IBAction func pressed_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tap_select_vehicle(_ sender: Any) {
        txt_select_vehicle.resignFirstResponder()
       // indexPath_val.removeAll()
       
        selected_values.removeAll()
        CallVehicleFromServer()
        view_table.isHidden = false
        isVehicleType = true
        
        UIView.animate(withDuration: 0.8, animations: {
            self.view_table.frame.origin.y = 20
        })
    }
    
    @IBAction func tap_alert_type(_ sender: Any) {
        txt_alert_type.resignFirstResponder()
        //indexPath_val.removeAll()
       
        selected_values.removeAll()
        CallAlertTypeData()
        view_table.isHidden = false
        isVehicleType = false
        //self.txt_vehicle_type.isEnabled = true
        
        UIView.animate(withDuration: 0.8, animations: {
            self.view_table.frame.origin.y = 20
        })
    }
    
    @IBAction func tap_from_date(_ sender: Any) {
        txt_from_date.resignFirstResponder()
     isFromDate = true
        view_date_picker_perent.isHidden = false
    }
    
    
    @IBAction func to_date_tap(_ sender: Any) {
        txt_to_date.resignFirstResponder()
        isFromDate = false
        view_date_picker_perent.isHidden = false
    }
    
    @IBAction func pressed_done_table(_ sender: Any) {
        
        if isVehicleType{
            
            for values in remove_values{
                if let index = vehicle_type_values.index(of: values) {
                    vehicle_type_values.remove(at: index)
                }
            }
            
            for values in indexPath_val_remove{
                if let index = indexPath_val.index(of: values) {
                    indexPath_val.remove(at: index)
                }
            }
        }else{
           
            for values in remove_values{
                if let index = alert_type_values.index(of: values) {
                    alert_type_values.remove(at: index)
                }
            }
            
            for values in indexPath_val_remove_alert{
                if let index = indexPath_val_alert.index(of: values) {
                    indexPath_val_alert.remove(at: index)
                }
            }
        }
        
                UIView.animate(withDuration: 0.8, animations: {
                    self.view_table.frame.origin.y = self.view_table.frame.size.height + 20
                    
                    if self.isVehicleType{
                        
                        if self.vehicle_type_values.count > 1{
                            let count = String(self.vehicle_type_values.count)
                            self.txt_select_vehicle.text = count + " Selected"
                        }else if self.vehicle_type_values.count > 0{
                            self.txt_select_vehicle.text = self.vehicle_type_values[0]
                        }else{
                            self.txt_select_vehicle.text =  "0 Selected"
                        }
                    }else{
                       
                        if self.alert_type_values.count == self.alert_data.count + 1{
                            
                            self.txt_alert_type.text = "All"
                        }else if self.alert_type_values.count > 1{
                            let count = String(self.alert_type_values.count)
                            self.txt_alert_type.text = count + " Selected"
                        }else if self.alert_type_values.count > 0{
                            self.txt_alert_type.text = self.alert_type_values[0]
                        }else{
                            self.txt_alert_type.text =  "0 Selected"
                        }
                    }
                   self.remove_values.removeAll()
                    self.indexPath_val_remove.removeAll()
                    self.indexPath_val_remove_alert.removeAll()
                })
    }
    
    @IBAction func select_day(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
        
           txt_to_date.text = TodayToDate()
           
         
           txt_from_date.text = TodayFromDate()
           
            if !view_todate_fromdate.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_todate_fromdate.isHidden = true
            }
          
            break
        case 1:
            
//            let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())
//            let formetter = DateFormatter()
//            formetter.dateFormat = "yyyy-MM-dd"
//            let result = formetter.string(from: yesterday!)
//            let fromdate = result + " 00:00:00"
            txt_from_date.text = YesterdayFromDate()
           // let todate = result + " 23:59:59"
            txt_to_date.text = YesterdayToDate()
            
            if !view_todate_fromdate.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_todate_fromdate.isHidden = true
            }
            break
        case 2:
           
            UIView.animate(withDuration: 0.5, animations: {
                self.view_todate_fromdate.frame.size.height = self.view_height
                self.btn_submit.frame.origin.y = self.button_origin_y
              
            })
            view_todate_fromdate.isHidden = false
            view_alerts_reports.isHidden = true
            
            break
        default:
            break;
        }
    }
    @IBAction func pressed_filter(_ sender: Any) {
        
        if view_alerts_reports.isHidden || view_alerts_reports.frame.origin.y == self.alert_report_origin{
            UIView.animate(withDuration: 0.5, animations: {
                self.view_flash.backgroundColor = UIColor.lightGray
            }, completion: { _ in
                self.view_flash.backgroundColor = UIColor.clear
                return
            })
            
        }
    btn_filter.setImage(UIImage(named: "filter"), for: .normal)
//        if view_alerts_reports.frame.origin.y == self.alert_report_origin{
//
//            UIView.animate(withDuration: 0.5, animations: {
//                self.view_alerts_reports.frame.origin.y = self.header_view.frame.size.height + 30
//                //self.view_alerts_reports.isHidden = false
//            })
//        }else{
        
            UIView.animate(withDuration: 0.5, animations: {
                self.view_alerts_reports.frame.origin.y =  self.alert_report_origin
                //self.view_alerts_reports.isHidden = true
            })
       // }
    }
    
    @IBAction func tap_search(_ sender: Any) {
        txt_search.resignFirstResponder()
        if view_alerts_reports.frame.origin.y == self.alert_report_origin{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_alerts_reports.frame.origin.y = self.txt_search.frame.origin.y + 35
                //self.view_alerts_reports.isHidden = false
            })
        }else{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_alerts_reports.frame.origin.y =  self.alert_report_origin
                //self.view_alerts_reports.isHidden = true
            })
        }
        
    }
    @objc func datePickerValueChanged(sender: UIDatePicker) {
       
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if isFromDate{
            txt_from_date.text = dateFormatter.string(from: sender.date)
           
        }else{
            txt_to_date.text = dateFormatter.string(from: sender.date)
           
        }
        
        
    }
    
 
   
    
    func CallAlertTypeData(){
        self.view.addSubview(alert_view)
        alert_data.removeAll()
        alert_type_id.removeAll()
        alert_data.append("All")
        alert_type_id.append("")
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let vehicle_type_url = domain_name + Alert_Type + "user_name=" + user_name + "&hash_key=" + hash_key
        
        CallTrackResult(urlString: vehicle_type_url, completionHandler: {data, r_error, isNetwork in
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Array<Any>
                    for val in c_data{
                        let v_val = val as! Dictionary<String, Any>
                        self.alert_data.append(v_val["alert_type_name"] as! String)
                        self.alert_type_id.append(v_val["alert_type_id"] as! String)
                    }
                    
                    self.filterdData = self.alert_data
                    self.tbl_shot_item.delegate = self
                    self.tbl_shot_item.dataSource = self
                    self.tbl_shot_item.reloadData()

                    break
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
                
            }else{
                showToast(controller: self, message: "Please check your Internet Connection.", seconds: 0.3)
                self.tbl_shot_item.isHidden = true
                print("ERROR FOUND")
            }
            self.alert_view.removeFromSuperview()
        })
    }
    
    func CallVehicleFromServer(){
        self.view.addSubview(alert_view)
        vehicle_data.removeAll()
        device_id.removeAll()
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let urlString = domain_name + Vehicle_Edit + "user_name=" + user_name + "&hash_key=" + hash_key

        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String,Any>
                    let val = c_data["return_json"] as! Array<Any>
                    
                    for name in val{
                        let v_name = name as! Dictionary<String, Any>
                        self.vehicle_data.append(v_name["registration_no"] as! String)
                        self.device_id.append(v_name["device_id"] as! String)
                    }
                        self.filterdData = self.vehicle_data
                        self.tbl_shot_item.delegate = self
                        self.tbl_shot_item.dataSource = self
                        self.tbl_shot_item.reloadData()
                    
                    break
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
                
            }else{
                showToast(controller: self, message: "Please check your Internet Connection.", seconds: 0.3)
              self.tbl_shot_item.isHidden = true
                print("ERROR FOUND")
            }
            self.alert_view.removeFromSuperview()
        })
    }
    
}

extension AlertReportVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return filterdData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tbl_shot_item.dequeueReusableCell(withIdentifier: "cell_vehicle") as! VehicleCell
        let cell = tbl_shot_item.dequeueReusableCell(withIdentifier: "cell_vehicle", for: indexPath) as! VehicleCell
        cell.lbl_item_name.text = filterdData[indexPath.row]
        
       // cell.accessoryType = .none
        
        if isVehicleType{
            if indexPath_val.count > 0{
                for val  in indexPath_val{
                    if indexPath.row == val{
                        cell.img_check.isHidden = false
                        //selected_values.append(self.data[indexPath.row])
                    }
                }
            }
        }else{
           
            for values in indexPath_val_remove_alert{
                if let index = indexPath_val_alert.index(of: values) {
                    indexPath_val_alert.remove(at: index)
                }
            }
            
            if indexPath_val_alert.count > 0{
                for val  in indexPath_val_alert{
                    
                    if indexPath.row == val{
                        cell.img_check.isHidden = false
                        //selected_values.append(self.data[indexPath.row])
                    }
                }
            }
            
            if alert_type_values.count < alert_data.count || remove_values.count > 0{
                if indexPath.row == 0{
                     cell.img_check.isHidden = true
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tbl_shot_item.cellForRow(at: indexPath) as! VehicleCell
        
        if cell.img_check.isHidden{
            cell.img_check.isHidden = false
            
            if isVehicleType{
                self.indexPath_val.append(indexPath.row)
                 //selected_values.append(self.vehicle_data[indexPath.row])
               
                vehicle_type_values.append(self.vehicle_data[indexPath.row])
            }else{
                
                if indexPath.row == 0{
                    indexPath_val_remove_alert.removeAll()
                    alert_type_values.removeAll()
                    remove_values.removeAll()
                    for (index, _) in alert_data.enumerated(){
                        
                        self.indexPath_val_alert.append(index)
                        alert_type_values.append(self.alert_data[index])
                    }
                    tbl_shot_item.reloadData()
                }else{
                  
                         self.indexPath_val_alert.append(indexPath.row)
                         alert_type_values.append(self.alert_data[indexPath.row])
                    for (index, val) in indexPath_val_remove_alert.enumerated(){
                        if val == indexPath.row{
                            remove_values.remove(at: index)
                            indexPath_val_remove_alert.remove(at: index)
                        }
                    }
                    
                   
                    
                  //selected_values.append(self.alert_data[indexPath.row])
                     tbl_shot_item.reloadData()
                }
                
              
            }
            
           
        }else{
            cell.img_check.isHidden = true
            
            if isVehicleType{
                 remove_values.append(self.vehicle_data[indexPath.row])
                self.indexPath_val_remove.append(indexPath.row)
            }else{
                
                if indexPath.row == 0{
                    indexPath_val_remove_alert.removeAll()
                    remove_values.removeAll()
                    for (index, _) in alert_data.enumerated(){
                        
                        self.indexPath_val_remove_alert.append(index)
                        remove_values.append(self.alert_data[index])
                    }
                    tbl_shot_item.reloadData()
                    
                }else{
                    
                    remove_values.append(self.alert_data[indexPath.row])
                    self.indexPath_val_remove_alert.append(indexPath.row)
                    for (index, val) in indexPath_val_alert.enumerated(){
                        if val == indexPath.row{
                             alert_type_values.remove(at: index)
                            indexPath_val_alert.remove(at: index)
                        }
                        
                    }
                  
                    tbl_shot_item.reloadData()
                }
                
            }
            
        }
        
//        if !isVehicleType{
//            if alert_data[indexPath.row] == "All"{
//                for (index, _) in alert_data.enumerated(){
//
//                    self.indexPath_val_alert.append(index)
//                    alert_type_values.append(self.alert_data[index])
//                }
//                tbl_shot_item.reloadData()
//            }
//        }
        

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_shot_item.frame.size.height / 8
    }
    
}

