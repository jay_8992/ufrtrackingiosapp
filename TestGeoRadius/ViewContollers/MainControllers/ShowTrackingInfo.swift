//
//  ShowTrackingInfo.swift
//  TestGeoRadius
//
//  Created by Georadius on 29/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import GoogleMaps
import Mapbox
import MapKit
import UIKit.UIGestureRecognizerSubclass


// MARK: - State

private enum State {
    case closed
    case open
}


extension State {
    var opposite: State {
        switch self {
        case .open: return .closed
        case .closed: return .open
        }
    }
}

class ShowTrackingInfo: UIViewController, MGLMapViewDelegate {
    
    @IBOutlet weak var btn_zoom_in: UIButton!
    @IBOutlet weak var btn_zoom_out: UIButton!
    
    @IBOutlet weak var btn_navigate: UIButton!
    @IBOutlet weak var btn_lock: UIButton!
    @IBOutlet weak var lbl_header_name: UILabel!
    @IBOutlet weak var tbl_vehicle_data: UITableView!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var view_show_history: UIView!
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var btn_traffic: UIButton!
    @IBOutlet weak var showMap: GMSMapView!
    @IBOutlet weak var map_show: UIView!
    @IBOutlet weak var map_VIew: MGLMapView!
    @IBOutlet weak var btn_share: UIButton!
    @IBOutlet weak var btn_croxx: UIButton!
    @IBOutlet weak var txt_password: UITextField!
    
    @IBOutlet weak var btn_done: UIButton!
    @IBOutlet weak var view_password: UIView!
    @IBOutlet weak var date_time_picker: UIDatePicker!
    @IBOutlet weak var view_date_and_time: UIView!
    
    var date_time : String!
    var mapView: MGLMapView!
     var rasterLayer: MGLRasterStyleLayer?
    var vehicals : Array<Any>?
    var latitude = [Double]()
    var longitude = [Double]()
    var start_date = [String]()
    var end_date = [String]()
    var index : Int!
    var registration = [String]()
    var vehicle_data = [NSAttributedString]()
    var data_heading = [String]()
    var device_serial = [String]()
    var vehicle_type_id = [String]()
    var vehicle_name = "car_green"
    var voice_no = [String]()
    var device_tag = [String]()
    var imageNamed : String = STOP_CAR
    var status = [Int]()
    var change_command_type : String!
    var device_id : String?
     let alert_view = AlertView.instanceFromNib()
    @IBOutlet var multiple_views: [UIView]!
    @IBOutlet var btn_view_change: [UIButton]!
    var containerViewController: TrackingStatusHistoryVC?
    var showReportTracking: ShowReportTracking?
    var showNotificationTracking: ShowNotificationTracking?
    var updateVehicle: UpdateVehicleVC?
    var iTemp:Int = 0
    var marker = GMSMarker()
    var rectangle = GMSPolyline()
    var timer = Timer()
    let path = GMSMutablePath()
     var direction = [Double]()
    var is_queue = false
   private let popupOffset: CGFloat = 300
    private var bottomConstraint = NSLayoutConstraint()
    var items_heading = ["Speed: ", "Distance: ", "Coordinates: ", "Halt Time: ", "Location: ", "Fuel: ", "Temprature: ", "Odometer: ", "Driver Name: ", "Driver Mobile: "]
    
    var imageName = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.addSubview(alert_view)
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        lbl_header_name.text = "Tracking"
        btn_traffic.addTarget(self, action: #selector(btn_traffic_pressed), for: .touchUpInside)
        btn_traffic.layer.cornerRadius = btn_traffic.frame.size.height / 10
        btn_traffic.layer.borderColor = UIColor.darkGray.cgColor
        btn_traffic.layer.borderWidth = 1
        
        btn_share.layer.cornerRadius = btn_share.frame.size.width / 2
        btn_share.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        
        btn_lock.layer.cornerRadius = btn_share.frame.size.width / 2
        btn_lock.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_lock.addTarget(self, action: #selector(CallIgnitionData), for: .touchUpInside)
        
        btn_zoom_in.layer.cornerRadius = btn_share.frame.size.width / 2
        btn_zoom_in.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_zoom_in.addTarget(self, action: #selector(pressed_zoom_in), for: .touchUpInside)

        btn_zoom_out.layer.cornerRadius = btn_share.frame.size.width / 2
        btn_zoom_out.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_zoom_out.addTarget(self, action: #selector(pressed_zoom_out), for: .touchUpInside)
        
        btn_navigate.layer.cornerRadius = btn_navigate.frame.size.width / 2
        btn_navigate.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_navigate.addTarget(self, action: #selector(openMapForPlace), for: .touchUpInside)
        
        btn_croxx.addTarget(self, action: #selector(cross_pressed), for: .touchUpInside)
        btn_done.addTarget(self, action: #selector(CallPasswordData), for: .touchUpInside)
        view_date_and_time.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_share.addTarget(self, action: #selector(ShareInfoToUser), for: .touchUpInside)
        date_time_picker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        mapView = MGLMapView(frame: view.bounds, styleURL: MGLStyle.lightStyleURL)
        mapView.frame = CGRect(x: 0, y: self.mapView.frame.origin.y, width: self.view.frame.size.width, height: self.mapView.frame.size.height)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.tintColor = .darkGray
        mapView.delegate = self
        
       // multiple_views[0].addSubview(map_VIew)
        map_show.addSubview(mapView)
        mapView.setCenter(CLLocationCoordinate2D(latitude: 28.6127, longitude: 77.2773), zoomLevel: 10, animated: true)
        let point = MyCustomPointAnnotation()
        point.coordinate = CLLocationCoordinate2D(latitude: 28.6127, longitude: 77.2773)
        let myPlaces = point
        mapView.addAnnotation(myPlaces)
        CallDataFromServerForIngnition(isDidLoad: false)
        
        // ------------------------------------------ Google Map ---------------------------------------- //
        self.showMap.animate(toLocation: CLLocationCoordinate2D(latitude: 28.6127, longitude: 77.2773))
        let camera = GMSCameraPosition.camera(withLatitude: 28.6127, longitude: 77.2773, zoom: 15)
        self.showMap!.camera = camera
        
        date_time = TodayToDate()
        self.tbl_vehicle_data.register(UINib(nibName: "Vehicle_Data_Cell", bundle: nil), forCellReuseIdentifier: "Vehicle_Data_Cell")
        SetAllData()
        
    }
    
    @IBAction func pressed_back_button(_ sender: Any) {
        StopAllThreads()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessVC") as! SuccessVC
        vc.delegate = self
        self.navigationController?.popViewController(animated: true)
      //  self.performSegue(withIdentifier: "status_seague", sender: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        is_queue = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        btn_lock.setImage(UIImage(named: "lock"), for: .normal)
        view_show_history.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        view_show_history.layer.shadowColor = UIColor.black.cgColor
        view_show_history.layer.shadowOpacity = 0.1
        view_show_history.layer.shadowRadius = 10
        view_show_history.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 3), radius: 5, scale: true)
        layout()
        overlayView.addGestureRecognizer(panRecognizer)

       // self.view.addSubview(alert_view)
        //NotificationCenter.default.addObserver(self, selector: #selector(GetTrackingData), name: Notification.Name("TrackingData"), object: nil)
        is_queue = true
        CallDataRepeatedly()

    }
    
    func layout(){
        view_show_history.translatesAutoresizingMaskIntoConstraints = false
        view_show_history.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        view_show_history.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        bottomConstraint = view_show_history.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: popupOffset)
        bottomConstraint.isActive = true
        view_show_history.heightAnchor.constraint(equalToConstant: 370).isActive = true
    }
    
    private var currentState: State = .closed
    
    private var runningAnimators = [UIViewPropertyAnimator]()
    
    private var animationProgress = [CGFloat]()
    
    private lazy var panRecognizer: InstantPanGestureRecognizer = {
        let recognizer = InstantPanGestureRecognizer()
        recognizer.addTarget(self, action: #selector(popupViewPanned(recognizer:)))
        
        return recognizer
    }()
    
    private func animateTransitionIfNeeded(to state: State, duration: TimeInterval) {
        
        guard runningAnimators.isEmpty else { return }
        
        let transitionAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1, animations: {
            switch state {
            case .open:
                self.bottomConstraint.constant = 0
                self.view_show_history.layer.cornerRadius = 20
                
            case .closed:
                self.bottomConstraint.constant = self.popupOffset
                self.view_show_history.layer.cornerRadius = 0
                
            }
            self.view.layoutIfNeeded()
        })
        
        transitionAnimator.addCompletion { position in
            
            switch position {
            case .start:
                self.currentState = state.opposite
            case .end:
                self.currentState = state
            case .current:
                ()
            }
            
            switch self.currentState {
            case .open:
                self.bottomConstraint.constant = 0
            case .closed:
                self.bottomConstraint.constant = self.popupOffset
            }
            
            self.runningAnimators.removeAll()
            
        }
        
        let inTitleAnimator = UIViewPropertyAnimator(duration: duration, curve: .easeIn, animations: {
            switch state {
            case .open: break
            case .closed: break
            }
        })
        inTitleAnimator.scrubsLinearly = false
        
        let outTitleAnimator = UIViewPropertyAnimator(duration: duration, curve: .easeOut, animations: {
            switch state {
            case .open: break
            case .closed: break
            }
        })
        outTitleAnimator.scrubsLinearly = false
        
        transitionAnimator.startAnimation()
        inTitleAnimator.startAnimation()
        outTitleAnimator.startAnimation()
        
        runningAnimators.append(transitionAnimator)
        runningAnimators.append(inTitleAnimator)
        runningAnimators.append(outTitleAnimator)
        
    }
    
    @objc private func popupViewPanned(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
            
        case .began:
            
            animateTransitionIfNeeded(to: currentState.opposite, duration: 1)
            
            runningAnimators.forEach { $0.pauseAnimation() }
            
            animationProgress = runningAnimators.map { $0.fractionComplete }
            
        case .changed:
            
            let translation = recognizer.translation(in: view_show_history)
            var fraction = -translation.y / popupOffset
            
            if currentState == .open { fraction *= -1 }
            if runningAnimators[0].isReversed { fraction *= -1 }
            
            for (index, animator) in runningAnimators.enumerated() {
                animator.fractionComplete = fraction + animationProgress[index]
            }
            
        case .ended:
            
            let yVelocity = recognizer.velocity(in: view_show_history).y
            let shouldClose = yVelocity > 0
            
            
            if yVelocity == 0 {
                runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
                break
            }
            
            switch currentState {
            case .open:
                if !shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                if shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
            case .closed:
                if shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                if !shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
            }
            
            runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
            
        default:
            ()
        }
    }
    
        override func performSegue(withIdentifier identifier: String, sender: Any?) {
            if identifier == "status_seague"{
               let vc = self.storyboard?.instantiateViewController(withIdentifier: "TrackingStatusHistoryVC") as! TrackingStatusHistoryVC
                vc.PlayOrPause()
            }
    
        }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //status_seague
        if segue.identifier == "status_seague"{
            containerViewController = segue.destination as? TrackingStatusHistoryVC
            containerViewController?.device_id = self.device_id
            containerViewController?.vehicle_type_id = self.vehicle_name
        }
        
        if segue.identifier == "report_seague"{
            showReportTracking = segue.destination as? ShowReportTracking
            showReportTracking?.device_id = self.device_id
        }
        
        if segue.identifier == "notification_seague"{
            showNotificationTracking = segue.destination as? ShowNotificationTracking
            showNotificationTracking?.device_id = self.device_id
        }

        //update_vehicle
    }
    
    @IBAction func pressed_map(_ sender: Any) {
        is_queue = true
        CallDataRepeatedly()
        multiple_views[1].tag = 1
        btn_view_change[0].tag = 0
        lbl_header_name.text = "Tracking"
        DidFunctionThings(buttonName: btn_view_change, viewName: multiple_views, buttonTagValue: 0, viewTagValue: 1)
    }
    
    @IBAction func pressed_notification(_ sender: Any) {
        is_queue = false
        multiple_views[3].tag = 3
        btn_view_change[3].tag = 3
        lbl_header_name.text = "Alert"
        DidFunctionThings(buttonName: btn_view_change, viewName: multiple_views, buttonTagValue: 3, viewTagValue: 3)
   }
    
    @IBAction func pressed_monitor(_ sender: Any) {
        is_queue = false
        multiple_views[4].tag = 4
        btn_view_change[4].tag = 4
        lbl_header_name.text = "Update Vehicle"
       //  self.performSegue(withIdentifier: "status_seague", sender: self)
        guard let MaplocationController = children[3] as? UpdateVehicleVC else  {
            fatalError("Check storyboard for missing SuccessVC \(String(describing: children[3]))")
        }
        
        updateVehicle = MaplocationController
        updateVehicle!.SetDataOnFields(registration_no: registration[index], voice_no: voice_no[index], device_tag: device_tag[index], device_serial: device_serial[index], device_id: device_id!, vehicle_type_id: vehicle_type_id[index])
        DidFunctionThings(buttonName: btn_view_change, viewName: multiple_views, buttonTagValue: 4, viewTagValue: 4)
    }
    
    @IBAction func presses_report(_ sender: Any) {
        is_queue = false
        multiple_views[0].tag = 0
        btn_view_change[1].tag = 1
         lbl_header_name.text = "Trips"
         //self.performSegue(withIdentifier: "status_seague", sender: self)
        DidFunctionThings(buttonName: btn_view_change, viewName: multiple_views, buttonTagValue: 1, viewTagValue: 0)
    }
    
    @IBAction func pressed_status(_ sender: Any) {
     is_queue = false
        multiple_views[2].tag = 2
        btn_view_change[2].tag = 2
       
       lbl_header_name.text = "Map History"
        // self.performSegue(withIdentifier: "status_seague", sender: self)
        DidFunctionThings(buttonName: btn_view_change, viewName: multiple_views, buttonTagValue: 2, viewTagValue: 2)
    }
    
    @IBAction func select_map_style(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex
        {
        case 0:
            showMap.mapType = .normal
            
        case 1:
            showMap.mapType = .satellite
            
        default:
            
            showMap.mapType = .hybrid
            
        }
    }
    
    
    @IBAction func date_time_done(_ sender: Any) {
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
           // self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        view_date_and_time.isHidden = true
        
      self.view.addSubview(alert_view)
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        //print("lslklks \(domain_name) \(hash_key) \(user_name) \(device_id!) \(date_time!)")
        let urlString1 = domain_name + "/vehicle_result.php?action=temp_access&device_id=" + device_id!
        let urlString = urlString1 + "&phone_val=&email_val=&data_format=1&user_name=" + user_name + "&hash_key=" + hash_key + "&validity_date=" + date_time
        
         let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: " ").inverted)
        
        CallShareDataFromServer(urlString: encodedString!, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                let url = data!["tempurl"] as! String
            
                let items = [url];
                let activity = UIActivityViewController(activityItems: items, applicationActivities: nil);
                self.present(activity, animated: true, completion: nil)
                
            }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
            
            self.alert_view.removeFromSuperview()
        })
    }
    
    func CallDataFromServerForIngnition( isDidLoad: Bool)
    {
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            //self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
         is_queue = false
       //self.view.addSubview(alert_view)
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        //let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        
        let urlString = domain_name + "/device_command_result.php?action=app_checkmobilize&data_format=1&device_id=" + device_id! + "&user_name=" + user_name + "&hash_key=" + hash_key
       // print("dlfkdlkfl \(urlString)")
        
        CallNotificatioSettingDataFromServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                    let command_type = val_data["command_type"] as! String
                    if command_type == "1100"{
                        
                        self.btn_lock.setImage(UIImage(named: "lock"), for: .normal)
                    }else{
                        self.btn_lock.setImage(UIImage(named: "unlock"), for: .normal)
                    }
                    self.change_command_type = command_type
//                    for (_, valu) in command_type.enumerated(){
//
//                        if valu == "1"{
//                             self.change_command_type.append("0")
//                        }else{
//                            self.change_command_type.append("1")
//                        }
//                    }
                }
                
            }else{
                if isDidLoad{
                    showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                }
                
                print("ERROR FOUND")
            }
            if r_error != nil{
                if isDidLoad{
                    showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                }
            }
            
           // self.alert_view.removeFromSuperview()
          
           // let locaked = String(self.change_command_type)
          self.is_queue = true
            self.CallDataRepeatedly()
        })
        
    }
    
    @objc func pressed_zoom_in(){
       
         let loc : CLLocation = CLLocation(latitude: latitude[index], longitude: longitude[index])
        let zoom_in = self.showMap.camera.zoom + 2
        let camera = GMSCameraPosition.camera(withTarget: loc.coordinate, zoom: zoom_in)
        self.showMap!.camera = camera
        
        //et camera = GMSCameraPosition.camera(withTarget: newLocation.coordinate, zoom: zoom)
        //self.showMap.animate(to: camera)
    }
    
    @objc func pressed_zoom_out(){
        let loc : CLLocation = CLLocation(latitude: latitude[index], longitude: longitude[index])
        let zoom_in = self.showMap.camera.zoom - 2
        let camera = GMSCameraPosition.camera(withTarget: loc.coordinate, zoom: zoom_in)
        self.showMap!.camera = camera
    }
    
    @objc func openMapForPlace() {
        
        let latitude: CLLocationDegrees = self.latitude[index]
        let longitude: CLLocationDegrees = self.longitude[index]
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
       // mapItem.name = "Place Name"
        mapItem.openInMaps(launchOptions: options)
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        date_time = dateFormatter.string(from: sender.date)
       
    }
    
    @objc func cross_pressed(){
        txt_password.text = ""
        view_password.isHidden = true
    }
    
    @objc func CallIgnitionData(){
        
        view_password.isHidden = false
    }
    
    @objc func CallPasswordData(){
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
           // self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
         is_queue = false
        if txt_password.text!.count < 1{
            showToast(controller: self, message: "Password Can't be empty", seconds: 2.0)
            return
        }
        
        let device_token = UserDefaults.standard.value(forKey: "DEVICE_TOKEN") as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        
        
        let urlString = domain_name + Authenticate_Url_Username + user_name + Authenticate_Url_Password + txt_password.text! + "&user_app_id=" + device_token
        
       
        CallWebService(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
                
               // print(String(describing: data))
                let result = data?[K_Result] as! Int
                print(result)
                switch (result){
                    
                case 0 :
                    //let c_data = data?[K_Data] as! Dictionary<String,Any>
                    //print("dlklkekjekj \(c_data)")
                    if self.change_command_type == "1100"{
                        self.change_command_type = "1101"
                    }else{
                        self.change_command_type = "1100"
                    }
                self.UpdateIgnitionData()
                    break
                    
                case 2 :
                   // let message = data?[K_Message] as! String
                   showToast(controller: self, message: "Something went wrong", seconds: 2.0)
                    //print(message)
                    break
                    
                default:
                    print("Default Case")
                }
            }else{
               showToast(controller: self, message: "Something went wrong", seconds: 2.0)
            }
       self.is_queue = true
            self.CallDataRepeatedly()
        })
    }
    
    func UpdateIgnitionData(){
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
           // self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
         is_queue = false
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        
      
        let urlString1  =  domain_name + Update_Ignition + String(change_command_type)
        
        let urlString2 = urlString1 + "&data_format=1&vehicle=" + self.device_id! + "&user_name=" + user_name
        
        let urlString = urlString2 + "&hash_key=" + hash_key
        
  //print("dfkldkl \(urlString)")
       
        CallUpdateDataOnServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                self.txt_password.text = ""
                  self.view_password.isHidden = true
                showToast(controller: self, message : data!, seconds: 2.0)
              
           
                if self.btn_lock.currentImage == UIImage(named: "lock"){
                      self.btn_lock.setImage(UIImage(named: "unlock"), for: .normal)
                } else{
                  
                      self.btn_lock.setImage(UIImage(named: "lock"), for: .normal)
                }
            }else{
                
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
            self.is_queue = true
            self.CallDataRepeatedly()
        })
        
    }
    
    @objc func btn_traffic_pressed(){
        if showMap.isTrafficEnabled{
            showMap.isTrafficEnabled = false
            btn_traffic.setImage(UIImage(named: "no_traffic"), for: .normal)
        }else{
            showMap.isTrafficEnabled = true
            btn_traffic.setImage(UIImage(named: "traffic"), for: .normal)
        }
    }
    
    
    
    @objc func ShareInfoToUser(){
  
        view_date_and_time.isHidden = false
        let date = Date()
        let formetter = DateFormatter()
        formetter.dateFormat = "yyyy-MM-dd HH:mm:ss"
         date_time_picker.minimumDate = date
        
    }
    
    func CallDataRepeatedly(){
        
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.tbl_vehicle_data.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let urlString = domain_name + Track_Base + "user_name=" + user_name + "&hash_key=" + hash_key
     
        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork{
                let result = data?[K_Result] as! Int
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String,Any>
                    self.vehicals = (c_data[VEHICALS] as! Array<Any>)
                    self.SetAllData()
                    break
                    
                case _ where result > 0 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
                
            }else{
               //showToast(controller: self, message: "Please check your Internet Connection.", seconds: 0.3)
                print("ERROR FOUND")
            }
            
            if self.is_queue{
                DispatchQueue.main.asyncAfter(deadline: .now() + 40.0) {
                    [weak self] in
                    if self?.is_queue != nil && (self?.is_queue)! {
                        self!.CallDataRepeatedly()
                    }
                }
            }
        })
    }
    
    
    
    @objc func GetTrackingData(notification: Notification){
        let c_data = notification.userInfo
        vehicals = (c_data![VEHICALS] as! Array<Any>)
        SetAllData()
    }
    
    
    func SetAllData(){
        latitude.removeAll()
        longitude.removeAll()
        registration.removeAll()
        status.removeAll()
        direction.removeAll()
        vehicle_type_id.removeAll()
        
        
        for (index, loc) in (vehicals?.enumerated())!{
            
            let val_data = loc as! Dictionary<String, Any>
            self.latitude.append(GetLatitudeFromData(Vehicals : val_data))
            self.longitude.append(GetLonitudeFromData(Vehicals: val_data))
            registration.append(GetRegistrationNumber(Vehicals: val_data))
            status.append(GetDeviceStatus(Vehicals: val_data))
            vehicle_type_id.append(GetVehicleTypeId(Vehicals: val_data))
            device_tag.append(val_data["device_tag"] as! String)
            device_serial.append(val_data["device_serial"] as! String)
            voice_no.append(val_data["voice_no"] as! String)
            let angle = Double(val_data["direction"] as! String)
            self.direction.append(angle!)
            
            if index == self.index{
                
                vehicle_data.removeAll()
                data_heading.removeAll()
                imageName.removeAll()
                if let speedwidkey = val_data["speed"] as? String {
                    let s_c_speed = NSAttributedString(string: speedwidkey + " Km/h")
                    vehicle_data.append(s_c_speed)
                    data_heading.append("Speed: ")
                    imageName.append("speed")
                }
                
                
                if let distance = val_data["today_distance"] as? Double{
                    let c_distance = String(distance)
                    let s_c_distance = NSAttributedString(string: c_distance + " Km")
                    vehicle_data.append(s_c_distance)
                    data_heading.append("Distance:")
                    imageName.append("distance")
                }
                
                let latitude = String(GetLatitudeFromData(Vehicals : val_data))
                let longitude = String(GetLonitudeFromData(Vehicals: val_data))
                
                let s_c_lat_long = NSAttributedString(string: latitude + ", " + longitude)
                vehicle_data.append(s_c_lat_long)
                data_heading.append("Coordinates: ")
                imageName.append("coordinate")
                
                if let halt_time = val_data["status_total_time"] as? String {
                    if halt_time.count > 0{
                        let s_c_halt_time = NSAttributedString(string: halt_time)
                        vehicle_data.append(s_c_halt_time)
                        data_heading.append("Halt Time: ")
                        imageName.append("halt_time")
                    }
                }
                
                if let location = val_data["location"] as? String{
                    let date = val_data["start_date"] as! String
                    let s_c_location = FixBoldBetweenText(firstString: location, boldFontName: " on ", lastString: date)
                    vehicle_data.append(s_c_location)
                    data_heading.append("Location: ")
                    imageName.append("location")
                }
                
                if let fuel = val_data["fuel_to_show"] as? String{
                    let s_c_fuel = NSAttributedString(string: fuel)
                    vehicle_data.append(s_c_fuel)
                    data_heading.append("Fuel: ")
                    imageName.append("fuel")
                }
                
                if let temperature = val_data["temperature"] as? String{
                    let s_c_temprature = NSAttributedString(string: temperature)
                    vehicle_data.append(s_c_temprature)
                    data_heading.append("Temperature: ")
                    imageName.append("temperature")
                }

                
                if let gps_odometer = val_data["gps_odometer"] as? String{
                    let s_c_gps_odometer = NSAttributedString(string: gps_odometer)
                    vehicle_data.append(s_c_gps_odometer)
                    data_heading.append("Odometer: ")
                    imageName.append("odometer")
                }
                
                if let driver_name = val_data["driver_name"] as? String{
                    let s_c_driver_name = NSAttributedString(string: driver_name)
                    vehicle_data.append(s_c_driver_name)
                    data_heading.append("Driver Name: ")
                    imageName.append("driver_name")
                }
                
                
                if let driver_mobile = val_data["driver_mobile"] as? String{
                    let s_c_driver_mobile = NSAttributedString(string: driver_mobile)
                    vehicle_data.append(s_c_driver_mobile)
                    data_heading.append("Driver Mobile: ")
                    imageName.append("driver_mobile")
                }
                
                tbl_vehicle_data.delegate = self
                tbl_vehicle_data.dataSource = self
                tbl_vehicle_data.reloadData()
                alert_view.removeFromSuperview()
            }
        }
        
        mapView.delegate = self
        SetDataOnMAp()
        playCar()
    }
    
    
    //--------------------------------------------- MapBox Delegate Methods ------------------------------------------------------//
   
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
        let source = MGLRasterTileSource(identifier: IDENTIFIER, tileURLTemplates: [Title_URL], options: [ .tileSize: 256 ])
        let rasterLayer = MGLRasterStyleLayer(identifier: IDENTIFIER, source: source)
        style.addSource(source)
        style.addLayer(rasterLayer)
        self.rasterLayer = rasterLayer
    }
    
    
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        guard annotation is MGLPointAnnotation else {
            return nil
        }
        
       /// let imageName = BIKE
        
        let reuseIdentifier = imageNamed
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        if annotationView == nil {
            annotationView = CustomImageAnnotationView(reuseIdentifier: reuseIdentifier, image: UIImage(named: imageNamed)!)
        }
        
        return annotationView
    }
    
    
    func mapView(_ mapView: MGLMapView, imageFor annotation: MGLAnnotation) -> MGLAnnotationImage? {
        if let castAnnotation = annotation as? MyCustomPointAnnotation {
            if (!castAnnotation.willUseImage) {
                return nil
            }
        }
        //let imageName = BIKE
        
        var annotationImage = mapView.dequeueReusableAnnotationImage(withIdentifier: imageNamed)
        if(annotationImage == nil) {
            annotationImage = MGLAnnotationImage(image: UIImage(named: imageNamed)!, reuseIdentifier: imageNamed)
        }
        return annotationImage
    }
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        
        return true
    }
    
    func mapView(_ mapView: MGLMapView, tapOnCalloutFor annotation: MGLAnnotation) {
        print("tap on callout")
    }
    
    func mapView(_ mapView: MGLMapView, didSelect annotationView: MGLAnnotationView) {
       // print("dfdfdfdffd")
    }
    


}

class InstantPanGestureRecognizer: UIPanGestureRecognizer {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        if (self.state == UIGestureRecognizer.State.began) { return }
        super.touchesBegan(touches, with: event)
        self.state = UIGestureRecognizer.State.began
    }
    
}

extension ShowTrackingInfo{
    func SetDataOnMAp(){
        
        
        // -----------------Remove Annotations -------------------------//
        
        self.mapView.annotations!.forEach {
            if !($0 is MKUserLocation) {
                self.mapView.removeAnnotation($0)
            }
        }
        
        //------------------------Add New Annotations ---------------------------//
        
        let point = MyCustomPointAnnotation()
        point.coordinate = CLLocationCoordinate2D(latitude: self.latitude[index], longitude: longitude[index])
       // point.title = self.registration[index]
       // print("skjksjkdj \(vehicle_type_id[index])")
        var vehicle_name = "car"
        if vehicle_type_id[index] == "56"{
            vehicle_name = "police"
        }else if vehicle_type_id[index] == "1"{
             vehicle_name = "car"
        }else if vehicle_type_id[index] == "2"{
            vehicle_name = "truck"
        }else if vehicle_type_id[index] == "3"{
            vehicle_name = "bus"
        }else if vehicle_type_id[index] == "4"{
            vehicle_name = "van"
        }else if vehicle_type_id[index] == "5"{
            vehicle_name = "car"
        }else if vehicle_type_id[index] == "6"{
            vehicle_name = "bike"
        }else if vehicle_type_id[index] == "7"{
            vehicle_name = "scooty"
        }else{
            vehicle_name = "car"
        }
        
        let myPlaces = point
        
        if status[index] == 0{
            
            imageNamed = vehicle_name + "_red"
        }else if status[index] == 1{
            
            imageNamed = vehicle_name + "_yellow"
        }else if status[index] == 2{
            
            imageNamed = vehicle_name + "_green"
        }else if status[index] == 4{
            
            imageNamed = vehicle_name + "_black"
        }
        mapView.addAnnotation(myPlaces)
        mapView.selectAnnotation(myPlaces, animated: true)
        alert_view.removeFromSuperview()
        
    }
    
    // ---------------------------------------- Google Map ------------------------------------------//
    
    func drawPathOnMap()  {
        
        path.add(CLLocationCoordinate2DMake(latitude[index], longitude[index]))
        
        rectangle = GMSPolyline(path: path)
        rectangle.strokeWidth = 5.0

        let styles = [GMSStrokeStyle.solidColor(.clear),
                      GMSStrokeStyle.solidColor(.black)]
        
        let lengths: [NSNumber] = [10, 10]
        rectangle.spans = GMSStyleSpans(rectangle.path!, styles, lengths,GMSLengthKind.rhumb)
        marker.map = showMap
        rectangle.map = showMap
        
    }
    
    
    func playCar()
      {
            print("index of location is :\(String(describing: index))")
            let loc : CLLocation = CLLocation(latitude: latitude[index], longitude: longitude[index])
            updateMapFrame(newLocation: loc, zoom: self.showMap.camera.zoom)
            marker.position = CLLocationCoordinate2DMake(latitude[index], longitude[index])
            marker.rotation = direction[index]
            marker.icon = UIImage(named: imageNamed)
            marker.setIconSize(scaledToSize: .init(width: 20, height: 40))
            marker.map = showMap
            drawPathOnMap()
    }
    
    func updateMapFrame(newLocation: CLLocation, zoom: Float) {
        let camera = GMSCameraPosition.camera(withTarget: newLocation.coordinate, zoom: zoom)
        self.showMap.animate(to: camera)
    }
}


extension ShowTrackingInfo: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return data_heading.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let info_cell = tbl_vehicle_data.dequeueReusableCell(withIdentifier: "Vehicle_Data_Cell") as! Vehicle_Data_Cell
        
        info_cell.lbl_heading.text = data_heading[indexPath.row]
        info_cell.img_heading.image = UIImage(named: imageName[indexPath.row])
        if indexPath.row == 0{
             info_cell.lbl_data.attributedText = vehicle_data[indexPath.row]
        }else if indexPath.row == 1{
             info_cell.lbl_data.attributedText = vehicle_data[indexPath.row]
        }else{
           info_cell.lbl_data.attributedText = vehicle_data[indexPath.row]
        }
        
        return info_cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
            return tbl_vehicle_data.frame .size.height / 9.5
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return tbl_vehicle_data.frame .size.height / 6.2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = Vehicle_Data_Header.instanceFromNib()
        //view.view_content.layer.cornerRadius = view.view_content.frame.size.height / 18
       // view.view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        view.lbl_registration.text = registration[index]
        return view
    }
    
    
}

extension ShowTrackingInfo: IsShowWait{
    func isWait() -> Bool {
        return true
    }
    
    
}
