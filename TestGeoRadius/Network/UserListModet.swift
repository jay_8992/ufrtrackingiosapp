//
//  UserListModet.swift
//  GeoTrack
//
//  Created by Georadius on 18/10/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import Foundation

class UserListModel {
    
    var user_name : String!
    var phone : String!
    var email : String!
    var user_id : String!
    
    init(user_name: String, phone: String, email: String, user_id: String) {
        self.user_name = user_name
        self.phone = phone
        self.email = email
        self.user_id = user_id
    }
}
