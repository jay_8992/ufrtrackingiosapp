//
//  TrackCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 25/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class TrackCell: UITableViewCell {
    @IBOutlet weak var lbl_regstration: UILabel!
    @IBOutlet weak var ing_ac: UIImageView!
    
    @IBOutlet weak var img_door: UIImageView!
    @IBOutlet weak var img_ignition: UIImageView!
    @IBOutlet weak var lbl_last_update: UILabel!
    @IBOutlet weak var lbl_coordinates: UILabel!
    @IBOutlet weak var lbl_halt_time: UILabel!
    @IBOutlet weak var lbl_location: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var lbl_speed: UILabel!
    @IBOutlet weak var img_vehicle: UIImageView!
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var alert_color: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        view_content.layer.cornerRadius = view_content.frame.size.height / 18
        view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)

     alert_color.layer.cornerRadius = 7
        alert_color.layer.masksToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
