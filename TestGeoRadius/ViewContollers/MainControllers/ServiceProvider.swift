//
//  ServiceProvider.swift
//  TestGeoRadius
//
//  Created by Georadius on 09/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class ServiceProvider: UIViewController {

    @IBOutlet weak var view_heaeder: UIView!
    @IBOutlet weak var txt_demo: UITextField!
    @IBOutlet weak var btn_menu_pressed: UIButton!
    @IBOutlet weak var txt_company_name: UITextField!
    @IBOutlet weak var txt_contact: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    
    let alert_view = AlertView.instanceFromNib()

    override func viewDidLoad() {
        super.viewDidLoad()
        SideMenu()
        // Do any additional setup after loading the view.
        view_heaeder.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)

        SetTextFieldLeftSide(imageName: "company_name", txt_field: txt_company_name)
        SetTextFieldLeftSide(imageName: "email", txt_field: txt_email)
        SetTextFieldLeftSide(imageName: "contact", txt_field: txt_contact)
        SetTextFieldLeftSide(imageName: "demo", txt_field: txt_demo)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        txt_demo.addTarget(self, action: #selector(tap_demo), for: .editingDidBegin)
        txt_contact.addTarget(self, action: #selector(tap_contact), for: .editingDidBegin)
        txt_email.addTarget(self, action: #selector(tap_email), for: .editingDidBegin)
        txt_company_name.addTarget(self, action: #selector(tap_company), for: .editingDidBegin)
        CallAPIToUploadData()
    }

    
    func SideMenu(){
        btn_menu_pressed.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        revealViewController()?.rearViewRevealWidth = 250
    }
    
    func CallAPIToUploadData(){
        
        self.view.addSubview(alert_view)
        
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.alert_view.removeFromSuperview()
            return
        }
        
        self.view.addSubview(alert_view)
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let group_id = UserDefaults.standard.value(forKey: GROUP_ID) as! String
        
        let urlString = domain_name + "/group_result.php?&action=search&group_name=&company_name=&group_id=" + group_id + "&user_name=" + user_name + "&hash_key=" + hash_key + "&data_format=1"
        
        CallNotificatioSettingDataFromServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                    self.txt_demo.text = val_data["group_name"] as? String
                    self.txt_contact.text = val_data["company_phone"] as? String
                    self.txt_email.text = val_data["company_email"] as? String
                    self.txt_company_name.text = val_data["company_name"] as? String
                }
                }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
               self.alert_view.removeFromSuperview()
        })
    }

}


extension ServiceProvider{
    
    @objc func tap_demo(){
        txt_demo.resignFirstResponder()
    }
    
    @objc func tap_contact(){
        txt_contact.resignFirstResponder()
    }
    
    @objc func tap_email(){
        txt_email.resignFirstResponder()
    }
    
    @objc func tap_company(){
        txt_company_name.resignFirstResponder()
    }
    
}
