//
//  AddLicenseVC.swift
//  GeoTrack
//
//  Created by Georadius on 30/07/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import Razorpay



class AddLicenseVC: UIViewController {

    @IBOutlet weak var view_enter_month: UIView!
    @IBOutlet weak var lbl_enter_months: UILabel!
    @IBOutlet weak var btn_buy_lciense: UIButton!
    @IBOutlet weak var lbl_total_amount: UILabel!
    @IBOutlet weak var lbl_amount_payble: UILabel!
    @IBOutlet weak var txt_months: UITextField!
    @IBOutlet weak var txt_vts_units: UITextField!
    @IBOutlet weak var tbl_drop_down: UITableView!
    @IBOutlet weak var btn_back: UIButton!
    @IBOutlet weak var view_header: UIView!
    let item_name = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
    let alert_view = AlertView.instanceFromNib()
    var unit_rate : String!
    var tax_name : String!
    var tax_value : String!
    var quote_scheme : String!
    var total_payble_amount_with_tax : String!
    var razorpay: Razorpay!
    var razorpayTestKey = ""
    var descriptions : String!
    var name : String!
    var amount_razorpay : String!
    var requestquote_id : String!
    var payment_id : String!
    var razorpay_paymentId : String!
    var order_id : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        tbl_drop_down.delegate = self
        tbl_drop_down.dataSource = self
        tbl_drop_down.isHidden = true
        
        
        btn_back.addTarget(self, action: #selector(btn_back_pressed), for: .touchUpInside)
        txt_months.addTarget(self, action: #selector(tap_months), for: .editingDidBegin)
        txt_vts_units.addTarget(self, action: #selector(tap_units), for: .editingDidEnd)
        btn_buy_lciense.addTarget(self, action: #selector(pressed_buy_license), for: .touchUpInside)
        
        view_enter_month.layer.cornerRadius = 5
        view_enter_month.layer.masksToBounds = true
        view_enter_month.layer.borderColor = UIColor.lightGray.cgColor
        view_enter_month.layer.borderWidth = 0.3
        
//        lbl_enter_months.layer.borderWidth = 1
//        lbl_enter_months.layer.cornerRadius = 5
//        lbl_enter_months.layer.masksToBounds = true
//        lbl_enter_months.layer.borderColor = UIColor.lightGray.cgColor
      
        
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tap_months))
        lbl_enter_months.isUserInteractionEnabled = true
        lbl_enter_months.addGestureRecognizer(tapgesture)
      
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        CallDataFromSever()
    }
    
   
    
    @objc func btn_back_pressed(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    internal func showPaymentForm(){
        let options: [String:Any] = [
            "amount": amount_razorpay!, //This is in currency subunits. 100 = 100 paise= INR 1.
            "currency": "INR",//We support more that 92 international currencies.
            "description": self.descriptions,
            //"image": "https://url-to-image.png",
            "order_id" : self.order_id!,
            "name": self.name,
            "prefill": [
                "contact": "",
                "email": ""
            ],
            "theme": [
                "color": "#F37254"
            ]
        ]
        razorpay.open(options)
    }
    
    
    func CallDataOfOrderSuccess(){
        self.view.addSubview(alert_view)
        if !NetworkAvailability.isConnectedToNetwork() {
            alert_view.removeFromSuperview()
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            return
        }
        
        
        
        let base_url = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String

        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        
        let url = base_url + "/billing_api.php"
     
        
        let parameters : [String : Any] = [ "action" : "order_success",
            "amount": total_payble_amount_with_tax!,
            "licence_quantity" : txt_vts_units.text!,
            "billing_term" : lbl_enter_months.text!,
            "user_name" : user_name,
            "hash_key" : hash_key,
            "razorpay_paymentId" :  self.razorpay_paymentId!,
            "requestquota_id" : requestquote_id!,
            "device_ids" : "",
            "payment_id" : payment_id!
        ]
        
        CallApiWithParameters(url: url, parameters: parameters, completion: {data, r_error, isNetwork in
            
            if isNetwork && data != nil{
                let val_data = data!.convertToDictionary()
                
                let message = val_data!["message"] as? String
                showToast(controller: self, message: message!, seconds: 1.5)
               
            }else{
                showToast(controller: self, message: "Something went wrong.", seconds: 0.5)
            }
            
            self.alert_view.removeFromSuperview()
        })
        
    }
    
    func CallDataForPayMent(){
        self.view.addSubview(alert_view)
        if !NetworkAvailability.isConnectedToNetwork() {
            alert_view.removeFromSuperview()
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            return
        }
        
 
        
     let base_url = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let base_url2 = UserDefaults.standard.value(forKey: "Domain_s") as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
       // let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        let url = base_url + "/billing_api.php"
       // print("sdlfklsdk \(url)")
        
        let parameters : [String : Any] = [   "action" : "payment_order",
            "amount" : total_payble_amount_with_tax!,
            "licence_quantity" : txt_vts_units.text!,
            "billing_term" : lbl_enter_months.text!,
            "domain_name" : base_url2,
            "firstname" : user_name,
            "productinfo" : "ADD " + txt_vts_units.text! + " VTS Licences for" +  lbl_enter_months.text! + " Month",
            "email" : "",
            "phone" : "",
            "user_name" : user_name,
            "hash_key" : hash_key,
            "device_ids" : ""
        ]
        
        
      //  print("jdfds \(parameters)")
        CallApiWithParameters(url: url, parameters: parameters, completion: {data, r_error, isNetwork in
            
            if isNetwork && data != nil{
                let val_data = data!.convertToDictionary()
                let result = val_data!["result"] as! Int
                //print("skldklk \(String(describing: val_data))")
                
                if result == 0{
                    let all_data = val_data!["data"] as! Dictionary<String, Any>
                   // print("skdjfkld \(all_data)")
                    self.amount_razorpay = String(all_data["amount"] as! Int)
                    self.razorpayTestKey = all_data["key"] as! String
                    self.descriptions = all_data["description"] as? String
                    self.name = all_data["name"] as? String
                    self.requestquote_id = all_data["requestquota_id"] as? String
                    self.payment_id = all_data["payment_id"] as? String
                    self.order_id = all_data["order_id"] as? String
                    self.razorpay = Razorpay.initWithKey(self.razorpayTestKey, andDelegate: self)
                   
                    self.showPaymentForm()
                }else{
                    showToast(controller: self, message: "Something went wrong.", seconds: 0.5)
                }
            }else{
                showToast(controller: self, message: "Something went wrong.", seconds: 0.5)
            }
            
            self.alert_view.removeFromSuperview()
        })
    }
    
   
    func CallDataFromSever(){
       
     self.view.addSubview(alert_view)
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            // self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
       
        self.view.addSubview(alert_view)
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
     
        let urlString = domain_name + "/billing_api.php?action=billing_details&user_name=" + user_name + "&hash_key=" +  hash_key
        
        let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: " ").inverted)
//print("dlfkdlk \(encodedString!)")
        CallShareDataFromServer(urlString: encodedString!, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
              
                self.unit_rate = data!["unit_rate"] as? String
                
                
                self.tax_name = GetTaxName(Vehicals: data!)
                    //data!["tax_name"] as? String
                self.tax_value = GetTaxValue(Vehicals: data!)
                    //data!["tax_value"] as? String
                self.quote_scheme = data!["quota_scheme"] as? String
                
                if self.tax_value == "0"{
                    self.lbl_amount_payble.text = "Amount Payble:   " + "₹0.0 " + self.tax_name + "(₹0)"

                }else{
                    self.lbl_amount_payble.text = "Amount Payble:   " + "₹0.0+" + self.tax_name + "@" + self.tax_value + "% (₹0)"

                }
         
                
            }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Domain Data Not Found.", seconds: 2.0)
            }
            
            self.alert_view.removeFromSuperview()
        })
    }
    
    func SendDataToServer(){
        
        self.view.addSubview(alert_view)

        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
     
        
        let urlString =  domain_name + "/vehicle_quota_result.php?action=add_device_quota&licence_quantity=" +  txt_vts_units.text! + "&billing_term=" + lbl_enter_months.text! + "&user_name=" + user_name +  "&hash_key=" + hash_key
        
        
        let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: " ").inverted)
        
       //  print("sdfkdkfjdk \(String(describing: encodedString))")
        
        CallUpdateDataOnServer(urlString: encodedString!, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                // print("kjkjkjk \(data!)")
                showToast(controller: self, message : data!, seconds: 2.0)
            }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                //print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
            self.alert_view.removeFromSuperview()
        })
    }
}

extension AddLicenseVC{
    
    @objc func pressed_buy_license(){
        
        if lbl_enter_months.text!.count < 1 || txt_vts_units.text!.count < 1{
            
            return
        }
        
        if self.quote_scheme == "1"{
           SendDataToServer()
            return
        }else if self.quote_scheme == "2"{
          //  showPaymentForm()
            CallDataForPayMent()
            return
        }
    }
    
    @objc func tap_units(){
        let unit = Double(txt_vts_units.text!) ?? 0
        
        let month = Double(lbl_enter_months.text!) ?? 0
        
        let amount = Double(self.unit_rate!) ?? 0
        
        let total_amount = (unit * amount) * month
      
       // lbl_total_amount.text = "₹ " + String(total_amount)
        

        var last_amount_all_tax_include : Double = 0.0
        let taxV = self.tax_value.components(separatedBy: ",")
        
        for val in taxV{
          
            let tax = Double(val) ?? 0
            let payble_amount = total_amount * tax
            let last_payble_amount = Double(payble_amount) / 100
            last_amount_all_tax_include = last_amount_all_tax_include + last_payble_amount
        }
       
        lbl_total_amount.text = "₹ " + String(last_amount_all_tax_include + total_amount)
        
        let last_amount_string = last_amount_all_tax_include.roundedtoPlaces(places: 3)
        
        let payble_amount_1 = "₹" + String(total_amount) + "+" + self.tax_name + "@"
        let payble_amount_2 = self.tax_value + "% (₹" + String(last_amount_string) + ")"
        self.lbl_amount_payble.text = "Amount Payble:   " + payble_amount_1 + payble_amount_2

        self.total_payble_amount_with_tax = String(last_amount_all_tax_include + total_amount)
    }
    
    @objc func tap_months(){
      
        txt_months.resignFirstResponder()
     
        tbl_drop_down.isHidden = false
    }
    
}

extension AddLicenseVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_drop_down.dequeueReusableCell(withIdentifier: "num_cell")
        cell?.textLabel?.text = item_name[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        lbl_enter_months.textColor = UIColor.black
        lbl_enter_months.text = item_name[indexPath.row]
        
        let unit = Double(txt_vts_units.text!) ?? 0
        
        let month = Double(lbl_enter_months.text!) ?? 0
        
        let amount = Double(self.unit_rate!) ?? 0
        
        let total_amount = (unit * amount) * month
     
        //= "₹ " + String(total_amount)
        
        var last_amount_all_tax_include : Double = 0.0
        let taxV = self.tax_value.components(separatedBy: ",")
       
        for val in taxV{
            
            let tax = Double(val) ?? 0
            let payble_amount = total_amount * tax
            let last_payble_amount = Double(payble_amount) / 100
            last_amount_all_tax_include = last_amount_all_tax_include + last_payble_amount
        }
        
        lbl_total_amount.text = "₹ " + String(last_amount_all_tax_include + total_amount)
         let last_amount_string = last_amount_all_tax_include.roundedtoPlaces(places: 3)
        
        let payble_amount_1 = "₹" + String(total_amount) + "+" + self.tax_name + "@"
        let payble_amount_2 = self.tax_value + "% (₹" + String(last_amount_string) + ")"
        self.lbl_amount_payble.text = "Amount Payble:   " + payble_amount_1 + payble_amount_2
        self.total_payble_amount_with_tax = String(last_amount_all_tax_include + total_amount)

        tbl_drop_down.isHidden = true
    }
    
   
    
}

extension AddLicenseVC : RazorpayPaymentCompletionProtocol{
    public func onPaymentError(_ code: Int32, description str: String){
        let alertController = UIAlertController(title: "FAILURE", message: str, preferredStyle: UIAlertController.Style.alert)
        let cancelAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    public func onPaymentSuccess(_ payment_id: String){
        
        self.razorpay_paymentId = payment_id
        
        let alertController = UIAlertController(title: "SUCCESS", message: "Payment Id \(payment_id)", preferredStyle: UIAlertController.Style.alert)
        let cancelAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        CallDataOfOrderSuccess()
    }
    
    
}
