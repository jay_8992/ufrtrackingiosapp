//
//  BillingVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 09/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class BillingVC: UIViewController {

    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var tbl_billing_reports: UITableView!
    
    var item_name = ["Add License", "Renew License", "Invoices", "Payments", "Statements"]
    var item_images = ["add_license", "renew_license", "invoice", "payment", "statement"]
    override func viewDidLoad() {
        super.viewDidLoad()
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)

        self.tbl_billing_reports.register(UINib(nibName: "Report_Billing_Cell", bundle: nil), forCellReuseIdentifier: "Report_Billing_Cell")
        self.tbl_billing_reports.delegate = self
        self.tbl_billing_reports.dataSource = self
        // Do any additional setup after loading the view.
    }
  
   

}
extension BillingVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return item_name.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_billing_reports.dequeueReusableCell(withIdentifier: "Report_Billing_Cell") as! Report_Billing_Cell
        cell.lbl_report_name.text = item_name[indexPath.row]
        cell.img_report.image = UIImage(named: item_images[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_billing_reports.frame.size.height / 6
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddLicenseVC") as! AddLicenseVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 1{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RenewLicenseVC") as! RenewLicenseVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 2{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 3{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 4{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "StatementVC") as! StatementVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
