//
//  Vehicle_Data_Header.swift
//  TestGeoRadius
//
//  Created by Georadius on 03/06/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class Vehicle_Data_Header: UIView {
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var img_ac: UIImageView!
    @IBOutlet weak var img_door: UIImageView!
    
    @IBOutlet weak var img_ignition: UIImageView!
    @IBOutlet weak var lbl_registration: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    class func instanceFromNib() -> Vehicle_Data_Header {
        let myClassNib = UINib(nibName: "Vehicle_Data_Header", bundle: nil)
        return myClassNib.instantiate(withOwner: nil, options: nil)[0] as! Vehicle_Data_Header
    }

}
