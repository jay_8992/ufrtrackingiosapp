//
//  GlobalName.swift
//  TestGeoRadius
//
//  Created by Georadius on 20/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import Foundation
import UIKit

let CountryID = "country_id"

let Global_Blue_Color : UIColor  = UIColor(red: 37.0/255, green: 159.0/255, blue: 220.0/255, alpha: 1.0)

let Global_Yellow_Color : UIColor = UIColor(red: 235.0/255, green: 213.0/255, blue: 32.0/255, alpha: 1.0)



let Moving_Color : UIColor = UIColor(red: 50.0/255, green: 205.0/255, blue: 50.0/255, alpha: 1.0)
//rgb(50,205,50)

let Stopped_Color : UIColor = UIColor.red

let Idle_Color : UIColor = UIColor.yellow

let Alert_Color : UIColor = UIColor.orange

let Unreach_Color : UIColor = UIColor.darkGray

let MOVING_CAR = "car_green"

let STOP_CAR = "car_red"

let UNREACHABLE_CAR = "car_black"

let IDLE_CAR = "car_yellow"

let BIKE = "bike"

let LoginKey = "loginKey"

let MOVING = "MOVING"

let STOPPED = "STOPPED"

let ALERT = "ALERT"

let UNREACH = "No Data"

let IDLE = "IDLE"

let ALL = "ALL"

let Extend = "extend"

let SIDEBAR_CELL = "SideBarCell"

let SUB_SIDEBAR_CELL = "SubSideBarCell"

let PLAY = "play"

let PAUSE = "pause"

//let razorpayTestKey = "rzp_test_9BcyssEVI5yoP6"

var mainVehicle : Array<Any>?

