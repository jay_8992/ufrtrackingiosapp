//
//  SettingViewController.swift
//  GeoTrack
//
//  Created by Georadius on 30/09/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SettingViewController: UIViewController {

    @IBOutlet weak var lbl_match_password_error: UILabel!
    @IBOutlet weak var btn_map_page_select: UIButton!
    @IBOutlet weak var btn_live_page_select: UIButton!
    @IBOutlet weak var btn_dashboard_select: UIButton!
    @IBOutlet weak var btn_home_cross: UIButton!
    @IBOutlet weak var btn_dashboard: UIButton!
    @IBOutlet weak var view_select_home_page: UIView!
    @IBOutlet weak var sub_view_select_home_page: UIView!
    @IBOutlet weak var lbl_error: UILabel!
    @IBOutlet weak var btn_reset_password: UIButton!
    @IBOutlet weak var txt_confirm_password: UITextField!
    @IBOutlet weak var txt_new_password: UITextField!
    @IBOutlet weak var btn_done: UIButton!
    @IBOutlet weak var btn_reenter: UIButton!
    @IBOutlet weak var btn_cress: UIButton!
    @IBOutlet weak var view_reset_password: UIView!
    @IBOutlet weak var sub_view_reset_password: UIView!
    @IBOutlet weak var btn_menu_pressed: UIButton!
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var btn_notification_setting: UIButton!
     let alert_view = AlertView.instanceFromNib()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        SideMenu()
         view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_notification_setting.addTarget(self, action: #selector(pressed_notification_setting), for: .touchUpInside)
        
        sub_view_reset_password.layer.cornerRadius = sub_view_reset_password.frame.size.height / 18
               sub_view_reset_password.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        
        sub_view_select_home_page.layer.cornerRadius = sub_view_select_home_page.frame.size.height / 18
        sub_view_select_home_page.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        
        btn_reenter.addTarget(self, action: #selector(re_enter_pressed), for: .touchUpInside)
        
        btn_reset_password.addTarget(self, action: #selector(pressed_reset_password), for: .touchUpInside)
        
        btn_cress.addTarget(self, action: #selector(pressed_cress_btn), for: .touchUpInside)
        
        btn_done.addTarget(self, action: #selector(pressed_done_btn), for: .touchUpInside)
        
        btn_dashboard.addTarget(self, action: #selector(pressed_dashboard), for: .touchUpInside)
        
        btn_home_cross.addTarget(self, action: #selector(pressed_home_cross_btn), for: .touchUpInside)
        
        btn_dashboard_select.addTarget(self, action: #selector(pressed_dashboard_select_btn), for: .touchUpInside)
        
        btn_map_page_select.addTarget(self, action: #selector(pressed_map_select_btn), for: .touchUpInside)
        
        btn_live_page_select.addTarget(self, action: #selector(pressed_live_select_btn), for: .touchUpInside)
        
        
        // Do any additional setup after loading the view.
    }
    

    @available(iOS 13.0, *)
    @objc func pressed_notification_setting(){
        let vc = self.storyboard?.instantiateViewController(identifier: "NotificationSettingVC") as! NotificationSettingVC
        self.navigationController?.pushViewController(vc, animated: true)
        print("Pressed Notifiation Setting")
    }


    func SideMenu(){
          btn_menu_pressed.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
          revealViewController()?.rearViewRevealWidth = 250
      }
    
    @objc func re_enter_pressed(){
        lbl_error.isHidden = true
        txt_new_password.text = ""
        txt_confirm_password.text = ""
    }
    
    @objc func pressed_reset_password(){
       
        view_reset_password.isHidden = false
    }
    
    @objc func pressed_cress_btn(){
        lbl_error.isHidden = true
        txt_new_password.text = ""
        txt_confirm_password.text = ""
        view_reset_password.isHidden = true
        lbl_match_password_error.isHidden = true
    }
    
    @objc func pressed_done_btn(){
        if txt_new_password.text!.count < 1 || txt_confirm_password.text!.count < 1{
            lbl_error.isHidden = false
            lbl_match_password_error.isHidden = true
            return
        }
        
        if txt_new_password.text != txt_confirm_password.text{
            lbl_match_password_error.isHidden = false
             lbl_error.isHidden = true
            return
        }
        lbl_match_password_error.isHidden = true
        lbl_error.isHidden = true

        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        
        let urlString = domain_name + Reset_Password + "user_name=" + user_name + "&" + "hash_key=" + hash_key + "&" + "user_id=" + user_id + "&" + "password=" + txt_new_password.text!
        
        alert_view.frame.size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(alert_view)
       // print("ldklskd \(urlString)")
        CallForgotPasswordAPI(urlString: urlString, completionHandler: {data, r_error, isNetwork in
                if isNetwork && data != nil{
//print("sdfkllsdk")
                    showToast(controller: self, message : data!, seconds: 2.0)
                    
                }else{
                   // print("sdfkllsdk \(isNetwork) \(String(describing: data))")
                    showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                    print("ERROR FOUND")
                }
                if r_error != nil{
                    showToast(controller: self, message : data!, seconds: 2.0)
                }
                
                self.alert_view.removeFromSuperview()
                self.LogoutFromServer()
            })
        
        
    }
    
    func LogoutFromServer(){
        print("Logout")
        UIApplication.shared.keyWindow!.addSubview(alert_view)
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
     
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        let device_token = UserDefaults.standard.value(forKey: "DEVICE_TOKEN") as! String
       
        
       //https://track.georadius.in/login_result.php?action=logoutApp&user_id=2212&user_app_id=4ccafb1053b6983e34d572d75388d920a5cb9ce8676577975d7a62cc8a3ceb81
        
        let urlString = domain_name + "/login_result.php?action=logoutApp&user_id=" + user_id + "&user_app_id=" + device_token
        
        CallUpdateDataOnServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                //showToast(controller: self, message : "Logout", seconds: 2.0)
            }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
            
            self.alert_view.removeFromSuperview()
            SaveLoginKey(key : "")
            let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDel.window?.rootViewController = loginVC
        })
        
    
    }
    
    @objc func pressed_dashboard(){
        view_select_home_page.isHidden = false
    }
    
    @objc func pressed_home_cross_btn(){
         view_select_home_page.isHidden = true
    }
    
    @objc func pressed_map_select_btn(){
        
        UserDefaults.standard.set(2, forKey: "select_home_page")
        UserDefaults.standard.set(true, forKey: "is_global_selection")
        btn_map_page_select.setImage(UIImage(named: "check"), for: .normal)
        
        btn_dashboard_select.setImage(UIImage(named: "uncheck"), for: .normal)
        
        btn_live_page_select.setImage(UIImage(named: "uncheck"), for: .normal)
        
    }
    
    @objc func pressed_live_select_btn(){
         UserDefaults.standard.set(1, forKey: "select_home_page")
        UserDefaults.standard.set(true, forKey: "is_global_selection")

               btn_map_page_select.setImage(UIImage(named: "uncheck"), for: .normal)
               
               btn_dashboard_select.setImage(UIImage(named: "uncheck"), for: .normal)
               
               btn_live_page_select.setImage(UIImage(named: "check"), for: .normal)
    }
    
    @objc func pressed_dashboard_select_btn(){
         UserDefaults.standard.set(0, forKey: "select_home_page")
        UserDefaults.standard.set(true, forKey: "is_global_selection")

                 btn_map_page_select.setImage(UIImage(named: "uncheck"), for: .normal)
                 
                 btn_dashboard_select.setImage(UIImage(named: "check"), for: .normal)
                 
                 btn_live_page_select.setImage(UIImage(named: "uncheck"), for: .normal)
      }
    
}


