//
//  ReportsVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 09/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit



class ReportsVC: UIViewController {

    @IBOutlet weak var tbl_report_items: UITableView!
    @IBOutlet weak var header_view: UIView!
    
    let name = ["Alerts", "Fleet Usage", "Over Speed", "Map History", "Trips", "Vehicle Distance", "Fuel", "Device Command", "Camera"]
    let images = ["alert.svg", "fleetusage.svg", "overspeed.svg", "history.svg", "trip.svg", "distance.svg", "fuel.svg", "device_command.svg", "camera.svg"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        header_view.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)

        // Do any additional setup after loading the view.
        self.tbl_report_items.register(UINib(nibName: "Report_Billing_Cell", bundle: nil), forCellReuseIdentifier: "Report_Billing_Cell")
        self.tbl_report_items.delegate = self
        self.tbl_report_items.dataSource = self
    }
    
    
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        if identifier == "alert_report"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AlertReportVC") as! AlertReportVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "fleet_usage"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FleetUsageVC") as! FleetUsageVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "overspeed"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OverspeedVC") as! OverspeedVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "map_history"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MapHistoryVC") as! MapHistoryVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "trip_report"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TripReportVC") as! TripReportVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "fuel_report"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FuelReportVC") as! FuelReportVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "vehicle_distance"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "VehicleDistanceVC") as! VehicleDistanceVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "CameraVC"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CameraVC") as! CameraVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "device_command"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DeviceCommandVC") as! DeviceCommandVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension ReportsVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return name.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_report_items.dequeueReusableCell(withIdentifier: "Report_Billing_Cell") as! Report_Billing_Cell
        cell.lbl_report_name.text = name[indexPath.row]
        //cell.img_report.image = UIImage(named: images[indexPath.row])
        let namSvgImgVar: SVGKImage = SVGKImage(named: images[indexPath.row])
        //let namSvgImgVyuVar = SVGKImageView(svgkImage: namSvgImgVar)
        let namImjVar: UIImage = namSvgImgVar.uiImage
        cell.img_report.image = namImjVar
        
        
       // cell.img_report.image = namImjVar
           // UIImage(named: namImjVar)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_report_items.frame.size.height / 7
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0{
            self.performSegue(withIdentifier: "alert_report", sender: self)
        }
        if indexPath.row == 1{
             self.performSegue(withIdentifier: "fleet_usage", sender: self)
        }
        if indexPath.row == 2{
            //
            self.performSegue(withIdentifier: "overspeed", sender: self)
        }
        if indexPath.row == 3{
            self.performSegue(withIdentifier: "map_history", sender: self)
        }
        if indexPath.row == 4{
            self.performSegue(withIdentifier: "trip_report", sender: self)
        }
        if indexPath.row == 5{
            self.performSegue(withIdentifier: "vehicle_distance", sender: self)
        }
        if indexPath.row == 6{
            self.performSegue(withIdentifier: "fuel_report", sender: self)
        }
        if indexPath.row == 7{
            self.performSegue(withIdentifier: "device_command", sender: self)
        }
        if indexPath.row == 8{
            self.performSegue(withIdentifier: "CameraVC", sender: self)
        }
        
    }
    
}
