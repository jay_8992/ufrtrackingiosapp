//
//  VehicleAssignment.swift
//  GeoTrack
//
//  Created by Georadius on 21/10/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class VehicleAssignment: UIViewController {

    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var btn_back: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        self.btn_back.addTarget(self, action: #selector(pressed_back_btn), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    @objc func pressed_back_btn(){
        self.navigationController?.popViewController(animated: true)
    }

}
