//
//  SuccessVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 20/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit


protocol IsShowWait {
    func isWait() -> Bool
   
}

class SuccessVC: UIViewController {

    @IBOutlet weak var tbl_show_live_track: UITableView!
    @IBOutlet weak var sarch_bar_item: UISearchBar!
    
    @IBOutlet weak var lbl_error: UILabel!
    @IBOutlet weak var collection_item: UICollectionView!
    var vehicals : Array<Any>?
    var vehicleData : Dictionary<String, Any>?
     let alert_view = AlertView.instanceFromNib()
    var moving = false
    var idle = false
    var stopped = false
    var no_data = false
    var all = true
  var vehicle_name = "car"
    var filterdData : [VehicleDetail] = []
     var searchActive : Bool = false
    var data : [VehicleDetail] = []
    var moving_c = 00
    var stopped_c = 00
    var unreach_c = 00
    var idle_c = 00
    var vehicals_s : Array<Any>?
    var timer = Timer()
    var delegate : IsShowWait?
    var is_queue = false
    var isBackground = false
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.tbl_show_live_track.register(UINib(nibName: "TrackCell", bundle: nil), forCellReuseIdentifier: "TrackCell")
 
        NotificationCenter.default.addObserver(self, selector: #selector(AtBackground), name:
            UIApplication.didEnterBackgroundNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AtForGround), name:
            UIApplication.willEnterForegroundNotification, object: nil)
        let searchBarStyle = sarch_bar_item.value(forKey: "searchField") as? UITextField
        searchBarStyle?.clearButtonMode = .never
        sarch_bar_item.placeholder = "Search"
        
       // sarch_bar_item.searchBarStyle = .minimal
        
      // tbl_show_live_track.reloadRows(at: tbl_show_live_track!.indexPathsForVisibleRows!, with: .none)

    }
 
    
    func CheckIsHidden(isHidden_V: Bool){
        if isHidden_V{
            is_queue = false
            StopAllThreads()

        }
    }
    
    
    @objc func AtBackground(){
        isBackground = true
    }
    
   @objc func AtForGround(){
        isBackground = false
    }
    
    func StartThread(isStart: Bool, vehic: Array<Any>){
       
        is_queue = isStart

        CallTrakingDataFromServer()
        
    }
    
    func SetAllDataToTable(vehicle_data: Array<Any>){
      
        if vehicle_data.count < 1{
            self.tbl_show_live_track.isHidden = true
            self.lbl_error.isHidden = false
            return
        }
      
        for val in vehicle_data{
            let val_data = val as! Dictionary<String, Any>
            let status = val_data["device_status"] as! Int
            if moving{
                if status == 2{
                    if self.moving_c < 1{
                        self.tbl_show_live_track.isHidden = true
                        self.lbl_error.isHidden = false
                        return
                    }else{
                        self.tbl_show_live_track.isHidden = false
                        self.lbl_error.isHidden = true
                        SetAllData(val_data: val_data)
                    }
                    
                }
            }else if idle{
               
                if status == 1{
                    if self.idle_c < 1 {
                        self.tbl_show_live_track.isHidden = true
                        self.lbl_error.isHidden = false
                        return
                    }else{
                        self.tbl_show_live_track.isHidden = false
                        self.lbl_error.isHidden = true
                        SetAllData(val_data: val_data)
                    }
                }
            }else if stopped{
               
                if status == 0{
                    if self.stopped_c < 1{
                        self.tbl_show_live_track.isHidden = true
                        self.lbl_error.isHidden = false
                        return
                    }else{
                        self.tbl_show_live_track.isHidden = false
                        self.lbl_error.isHidden = true
                        SetAllData(val_data: val_data)
                    }
                }
            }else if no_data{
                if status == 4{
                    if self.unreach_c < 1{
                        self.tbl_show_live_track.isHidden = true
                        self.lbl_error.isHidden = false
                        return
                    }else{
                        self.tbl_show_live_track.isHidden = false
                        self.lbl_error.isHidden = true
                        SetAllData(val_data: val_data)
                    }
                }
            }else if all{
                self.tbl_show_live_track.isHidden = false
                self.lbl_error.isHidden = true
                SetAllData(val_data: val_data)
            }
        }
    }
    
    
    func Check(Moving: Bool, Idle: Bool, Stopped: Bool, No_data: Bool, All: Bool, select: Bool, vehicle_data: Array<Any>){
        
        is_queue = true
        moving = Moving
        idle = Idle
        stopped = Stopped
        no_data = No_data
        all = All
        self.tbl_show_live_track.isHidden = true
        self.CallTrakingDataFromServer()
     // SetAllDataToTable(vehicle_data: vehicle_data)
        
    }
    
    func SetAllData(val_data: Dictionary<String, Any>){
     
       let registration = GetRegistrationNumber(Vehicals: val_data)
        let latitude = GetLatitudeFromData(Vehicals : val_data)
        let longitude = GetLonitudeFromData(Vehicals: val_data)
        let distance = val_data["today_distance"] as! Double
        let status = val_data["device_status"] as! Int
        let speed = val_data["speed"] as! String
        let location = val_data["location"] as! String
        let halt_time = val_data["status_total_time"] as! String
        let last_update = val_data["last_update"] as! String
        let ignition_status = val_data["ignition_status"] as! String
        
        
        let vehicle_type_id = GetVehicleTypeId(Vehicals: val_data)
        let digital_sensor_status = val_data["digital_sensor_status"] as! String
        let device_id = GetDeviceID(Vehicals: val_data)
        let newDetail = VehicleDetail(registration: registration, speed: speed, location: location, distance: distance, status: status, latitude: latitude, device_id: device_id, longitude: longitude, halt_time: halt_time, last_update: last_update, ignition_status: ignition_status, digital_sensor_status: digital_sensor_status, vehicle_type_id: vehicle_type_id)

            self.data.append(newDetail)
            self.tbl_show_live_track.isHidden = false
            self.tbl_show_live_track.delegate = self
            self.tbl_show_live_track.dataSource = self
        
        let contentOffset = self.tbl_show_live_track.contentOffset
        
        self.tbl_show_live_track.reloadData()
        
        self.tbl_show_live_track.setContentOffset(contentOffset, animated: true)
            self.sarch_bar_item.delegate = self
        
            filterdData = data

        UIView.animate(withDuration: 0.2, animations: {
            self.alert_view.removeFromSuperview()
        })
    }
    
    
    func CallTrakingDataFromServer(){
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.tbl_show_live_track.isHidden = true
            self.lbl_error.isHidden = false
            self.alert_view.removeFromSuperview()
            return
        }
        self.view.addSubview(self.alert_view)
        self.alert_view.backgroundColor = UIColor.clear
        
        if !isBackground{
            self.vehicals?.removeAll()
            self.vehicals_s?.removeAll()
        }
       
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let urlString = domain_name + Track_Base + "user_name=" + user_name + "&hash_key=" + hash_key
        
        
        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
          
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    
                    let c_data = data?[K_Data] as! Dictionary<String,Any>
                    self.vehicals = (c_data["vehicles"] as! Array<Any>)
                    self.moving_c = c_data["moving_count"] as! Int
                    self.stopped_c =  c_data["stopped_count"] as! Int
                    self.unreach_c =  c_data["unreachable_count"] as! Int
                    self.idle_c =  c_data["idling_count"] as! Int
                    self.vehicals_s = (c_data[VEHICALS] as! Array<Any>)
                    self.data.removeAll()
                    
                    self.SetAllDataToTable(vehicle_data: self.vehicals!)

                    let allData : Dictionary<String, Any> = ["moving_count" : self.moving_c,
                                                             "stopped_count" : self.stopped_c,
                                                             "unreachable_count" : self.unreach_c,
                                                             "idling_count" : self.idle_c
                    ]
                    
                    NotificationCenter.default.post(name: Notification.Name("TrackingDataForDashboard"), object: nil, userInfo: allData)
                    
                    break
                    
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
            }else{
                
                //Added 3/12/2019
                
                if let vehicleCount = self.vehicals?.count
                {
                    if vehicleCount < 1
                    {
                        self.tbl_show_live_track.isHidden = true
                        self.lbl_error.isHidden = false
                    }
                    else
                    {
                        self.data.removeAll()
                        self.SetAllDataToTable(vehicle_data: self.vehicals!)
                    }
                }
                else
                {
                      self.tbl_show_live_track.isHidden = true
                      self.lbl_error.isHidden = false
                }
                
                
//                if self.vehicals!.count < 1{
//                    self.tbl_show_live_track.isHidden = true
//                    self.lbl_error.isHidden = false
//                    //print("jjhjhjhyuuuuuuERROR FOUND")
//                }else{
//                    self.data.removeAll()
//                    self.SetAllDataToTable(vehicle_data: self.vehicals!)
//                      //print("jjhjhjhyuuuuuuERROR FOUND ------------")
//                }
                
             
            }
            if self.is_queue{
            DispatchQueue.main.asyncAfter(deadline: .now() + 50.0) {
               
                [weak self] in
                if self?.is_queue != nil && (self?.is_queue)! {
//                self!.view.addSubview(self!.alert_view)
//                self!.alert_view.backgroundColor = UIColor.clear
                self!.CallTrakingDataFromServer()
                }
             }
            }
      
            self.alert_view.removeFromSuperview()
        })
    }
    
    @objc func RepeatFunc(){
        self.vehicals?.removeAll()
        //data.removeAll()
        CallTrakingDataFromServer()
    }
 
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func pressed_back(_ sender: Any) {
    
        let side_bar = SideBar.instanceFromNib()
         self.view.addSubview(side_bar)
        
    }
    
    func SetDataInCell(cell: TrackCell, indexPath: Int, data: [VehicleDetail]) -> UITableViewCell{
        
        cell.lbl_regstration.text = data[indexPath].registration
        cell.lbl_speed.text = data[indexPath].speed + " Km/h"
        cell.lbl_location.text = data[indexPath].location
        cell.lbl_distance.text = String(data[indexPath].distance) + " Km"
        
        if data[indexPath].halt_time.count > 0{
            cell.lbl_halt_time.text = data[indexPath].halt_time
        }else{
            cell.lbl_halt_time.text = "-----"
        }
        
        if data[indexPath].ignition_status == "0"{
            cell.img_ignition.image = UIImage(named: "ignition_off")
        }else{
            cell.img_ignition.image = UIImage(named: "ignition_on")
        }
        
        let get_status = data[indexPath].digital_sensor_status.suffix(2)
        
        let get_ac_status = get_status.suffix(1)
        
        let get_door_status = get_status.prefix(1)
        
        if get_ac_status == "0"{
            cell.ing_ac.image = UIImage(named: "ac_off")
        }else{
            cell.ing_ac.image = UIImage(named: "ac_on")
        }
        
        if get_door_status == "0"{
            cell.img_door.image = UIImage(named: "door_close")
        }else{
            cell.img_door.image = UIImage(named: "door_open")
        }
        
        
        cell.lbl_last_update.text = data[indexPath].last_update
        cell.lbl_coordinates.text = String(data[indexPath].latitude) + ", " + String(data[indexPath].longitude)
        
        
        if data[indexPath].vehicle_type_id == "56"{
            vehicle_name = "police"
        }else if data[indexPath].vehicle_type_id == "1"{
            vehicle_name = "car"
        }else if data[indexPath].vehicle_type_id == "2"{
            vehicle_name = "truck"
        }else if data[indexPath].vehicle_type_id == "3"{
            vehicle_name = "bus"
        }else if data[indexPath].vehicle_type_id == "4"{
            vehicle_name = "van"
        }else if data[indexPath].vehicle_type_id == "5"{
            vehicle_name = "car"
        }else if data[indexPath].vehicle_type_id == "6"{
            vehicle_name = "bike"
        }else if data[indexPath].vehicle_type_id == "7"{
            vehicle_name = "scooty"
        }else{
            vehicle_name = "car"
        }
        
        
        if data[indexPath].status == 0{
            cell.alert_color.backgroundColor = UIColor.red
            cell.img_vehicle.image = UIImage(named: vehicle_name + "_red")
        }
        if data[indexPath].status == 1{
            cell.alert_color.backgroundColor = UIColor.yellow
            cell.img_vehicle.image = UIImage(named: vehicle_name + "_yellow")
        }
        if data[indexPath].status == 2{
            cell.alert_color.backgroundColor = UIColor.green
            cell.img_vehicle.image = UIImage(named: vehicle_name + "_green")
        }
        if data[indexPath].status == 4{
            cell.alert_color.backgroundColor = UIColor.black
            cell.img_vehicle.image = UIImage(named: vehicle_name + "_black")
        }
        //alert_view.removeFromSuperview()
        return cell
    }
    
}

extension SuccessVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
            return filterdData.count
        } else {
            return data.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_show_live_track.dequeueReusableCell(withIdentifier: "TrackCell") as! TrackCell
        
        if searchActive{
               return SetDataInCell(cell: cell, indexPath: indexPath.row, data: filterdData)
        }else{
           return SetDataInCell(cell: cell, indexPath: indexPath.row, data: data)
        }
        
    
       
      
//        if indexPath.row == 0{
//            cell.alert_color.backgroundColor =  Moving_Color
//             cell.img_vehicle.image = UIImage(named: CAR)
//
//        }
//        if indexPath.row == 1{
//            cell.alert_color.backgroundColor =  Alert_Color
//            cell.img_vehicle.image = UIImage(named: BIKE)
//        }
//        if indexPath.row == 2{
//            cell.alert_color.backgroundColor =  Stopped_Color
//             cell.img_vehicle.image = UIImage(named: CAR)
//        }
//        if indexPath.row == 3{
//            cell.alert_color.backgroundColor =  Unreach_Color
//             cell.img_vehicle.image = UIImage(named: BIKE)
//        }
//        if indexPath.row == 4{
//            cell.alert_color.backgroundColor =  Idle_Color
//             cell.img_vehicle.image = UIImage(named: BIKE)
//        }
//
        
    }
    
   

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_show_live_track.frame.size.height / 1.8
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.is_queue = false
        StopAllThreads()
        var index_val : Int!
        var deviceID = [String]()
        UserDefaults.standard.set(false, forKey: "is_global_selection")
       
        
        for (index, loc) in (vehicals?.enumerated())!{
            let val_data = loc as! Dictionary<String, Any>
            deviceID.append(GetDeviceID(Vehicals: val_data))
            if(searchActive) {
                if filterdData[indexPath.row].registration == GetRegistrationNumber(Vehicals: val_data){
                        index_val = index
                }}
            else {
                    if data[indexPath.row].registration == GetRegistrationNumber(Vehicals: val_data){
                        index_val = index
                    }
               }
           }
        
        self.sarch_bar_item.endEditing(true)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShowTrackingInfo") as! ShowTrackingInfo
        vc.index = index_val
        vc.vehicals = vehicals_s
        vc.device_id = deviceID[index_val]
        vc.vehicle_name = vehicle_name + "_green"
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//extension SuccessVC : UICollectionViewDelegate, UICollectionViewDataSource{
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 4
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collection_item.dequeueReusableCell(withReuseIdentifier: "CollectionItems", for: indexPath) as! CollectionItems
//        cell.layer.cornerRadius = cell.frame.size.height / 2
//
//        if indexPath.item == 0{
//            cell.backgroundColor = Moving_Color
//            cell.lbl_name.textColor = UIColor.white
//            cell.lbl_count.textColor = UIColor.white
//            cell.lbl_name.text = MOVING
//        }
//        if indexPath.item == 1{
//            cell.backgroundColor = Stopped_Color
//            cell.lbl_name.textColor = UIColor.white
//            cell.lbl_count.textColor = UIColor.white
//            cell.lbl_name.text = STOPPED
//        }
//
//        if indexPath.item == 2{
//            cell.backgroundColor = Unreach_Color
//            cell.lbl_name.textColor = UIColor.white
//            cell.lbl_count.textColor = UIColor.white
//            cell.lbl_name.text = UNREACH
//        }
//        if indexPath.item == 3{
//            cell.backgroundColor = Idle_Color
//            cell.lbl_name.textColor = UIColor.black
//            cell.lbl_count.textColor = UIColor.black
//            cell.lbl_name.text = IDLE
//        }
//
//
//        return cell
//    }
//
//
//}

extension SuccessVC: UISearchBarDelegate{
    
    
    
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//
//        filterdData = searchText.isEmpty ? registration : vehicals!.filter { $0.registration.contains(searchText) }
//
//        //return item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
//        tbl_show_live_track.reloadData()
//
//    }
//
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        filterdData = registration
//        tbl_show_live_track.reloadData()
//   }
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        self.sarch_bar_item.showsCancelButton = true
//    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        self.sarch_bar_item.endEditing(true)
       // self.tbl_show_live_track.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        
        self.sarch_bar_item.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        
        self.sarch_bar_item.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filterdData = data.filter { $0.registration.localizedCaseInsensitiveContains(searchText) }
        if(filterdData.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
      
        self.tbl_show_live_track.reloadData()
    }
}

