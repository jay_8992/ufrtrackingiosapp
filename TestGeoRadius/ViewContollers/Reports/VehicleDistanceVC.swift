//
//  VehicleDistanceVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 20/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class VehicleDistanceVC: UIViewController {

    @IBOutlet weak var btn_filter: UIButton!
    @IBOutlet weak var view_flash: UIView!
    @IBOutlet weak var view_date_time_perent: UIView!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var view_vehicle_report: UIView!
    @IBOutlet weak var header_view: UIView!
    @IBOutlet weak var date_and_time_picker: UIDatePicker!
    @IBOutlet weak var tbl_sort: UITableView!
    @IBOutlet weak var view_yable: UIView!
    @IBOutlet weak var lbl_group_time: UILabel!
    @IBOutlet weak var txt_to_date_and_time: UITextField!
    @IBOutlet weak var txt_from_date_time: UITextField!
    @IBOutlet weak var view_date_and_time: UIView!
    @IBOutlet weak var slider_distance: UISlider!
    @IBOutlet weak var txt_select_vehicle: UITextField!
    
    var alert_report_origin : CGFloat!
     var isFromDate = true
    var view_height : CGFloat!
    var button_origin_y : CGFloat!
    var filterdData : [String]!
    var vehicle_data = [String]()
    var device_id = [String]()
    var remove_values = [String]()
    var indexPath_val = [Int]()
    var indexPath_val_remove = [Int]()
      var vehicle_type_values = [String]()
    var report_data_controller : VehicleDistanceReportVC?
    var alert_view = AlertView.instanceFromNib()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        header_view.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        date_and_time_picker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        btn_submit.addTarget(self, action: #selector(submit_pressed), for: .touchUpInside)
        slider_distance.addTarget(self, action: #selector(updateKmsLabel), for: .allEvents)
 slider_distance.setValue(24, animated: true)
        self.lbl_group_time.text = "Grouping Hours: 24"
        self.tbl_sort.register(UINib(nibName: "VehicleCell", bundle: nil), forCellReuseIdentifier: "cell_vehicle")
        alert_report_origin = view_vehicle_report.frame.origin.y
        SetTextFieldLeftSide(imageName: "search", txt_field: txt_search)
        
        txt_to_date_and_time.text = TodayToDate()
        
        txt_from_date_time.text = TodayFromDate()
        
        SetReportView(viewName: view_vehicle_report)
        SetOrigin()
    }
    
    func SetOrigin(){
        
        view_height = view_date_and_time.frame.size.height
        
        self.view_date_and_time.frame.size.height = 0
        
        button_origin_y = btn_submit.frame.origin.y
        
        btn_submit.frame.origin.y =  view_date_and_time.frame.origin.y + 30.0
    }
    
    
    @IBAction func pressed_filter(_ sender: Any) {
        
        if view_vehicle_report.isHidden || view_vehicle_report.frame.origin.y == self.alert_report_origin{
            UIView.animate(withDuration: 0.5, animations: {
                self.view_flash.backgroundColor = UIColor.lightGray
            }, completion: { _ in
                self.view_flash.backgroundColor = UIColor.clear
                return
            })
            
        }
        
        btn_filter.setImage(UIImage(named: "filter"), for: .normal)
        
//        if view_vehicle_report.frame.origin.y == self.alert_report_origin{
//
//            UIView.animate(withDuration: 0.5, animations: {
//                self.view_vehicle_report.frame.origin.y = self.header_view.frame.size.height + 30
//                //self.view_alerts_reports.isHidden = false
//            })
//        }else{
        
            UIView.animate(withDuration: 0.5, animations: {
                self.view_vehicle_report.frame.origin.y =  self.alert_report_origin
                //self.view_alerts_reports.isHidden = true
            })
        //}
    }
    
    @IBAction func tap_search(_ sender: Any) {
        txt_search.resignFirstResponder()
        if view_vehicle_report.frame.origin.y == self.alert_report_origin{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_vehicle_report.frame.origin.y = self.txt_search.frame.origin.y + 35
                //self.view_alerts_reports.isHidden = false
            })
        }else{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_vehicle_report.frame.origin.y =  self.alert_report_origin
                //self.view_alerts_reports.isHidden = true
            })
        }
    }
    
    @IBAction func btn_date_done(_ sender: Any) {
        view_date_time_perent.isHidden = true
    }
    
    @IBAction func back_pressed(_ sender: Any) {
    
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tap_select_vehicle(_ sender: Any) {
        txt_select_vehicle.resignFirstResponder()
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.tbl_sort.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        CallVehicleFromServer()
        view_yable.isHidden = false
        UIView.animate(withDuration: 0.8, animations: {
            self.view_yable.frame.origin.y = 20
        })
    }
    
    @IBAction func day_select(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            txt_to_date_and_time.text = TodayToDate()
            txt_from_date_time.text = TodayFromDate()
            if !view_date_and_time.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_date_and_time.isHidden = true
            }
            
            break
        case 1:
            
            txt_from_date_time.text = YesterdayFromDate()
            txt_to_date_and_time.text = YesterdayToDate()
            
            if !view_date_and_time.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_date_and_time.isHidden = true
            }
            break
        case 2:
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_date_and_time.frame.size.height = self.view_height
                self.btn_submit.frame.origin.y = self.button_origin_y
            })
            view_date_and_time.isHidden = false
            view_vehicle_report.isHidden = true
            break
        default:
            break;
        }
    }
    
    @IBAction func tap_from_date(_ sender: Any) {
        txt_from_date_time.resignFirstResponder()
        isFromDate = true
        view_date_time_perent.isHidden = false
    }
    
    @IBAction func tap_to_date(_ sender: Any) {
        txt_to_date_and_time.resignFirstResponder()
        isFromDate = false
        view_date_time_perent.isHidden = false
    }
    
    @IBAction func pressed_done(_ sender: Any) {
        
        for values in remove_values{
            if let index = vehicle_type_values.index(of: values) {
                vehicle_type_values.remove(at: index)
            }
        }
        
        for values in indexPath_val_remove{
            if let index = indexPath_val.index(of: values) {
                indexPath_val.remove(at: index)
            }
        }
        
        
        UIView.animate(withDuration: 0.8, animations: {
            self.view_yable.frame.origin.y = self.view_yable.frame.size.height + 20
            
            
            if self.vehicle_type_values.count > 1{
                let count = String(self.vehicle_type_values.count)
                self.txt_select_vehicle.text = count + " Selected"
            }else if self.vehicle_type_values.count > 0{
                self.txt_select_vehicle.text = self.vehicle_type_values[0]
            }else{
                self.txt_select_vehicle.text =  "0 Selected"
            }
            
            self.remove_values.removeAll()
            self.indexPath_val_remove.removeAll()
            
        })
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if isFromDate{
            txt_from_date_time.text = dateFormatter.string(from: sender.date)
        
        }else{
            txt_to_date_and_time.text = dateFormatter.string(from: sender.date)
           
        }
        
        
    }
    
    @objc func submit_pressed(){
        
        if txt_to_date_and_time.text! <  txt_from_date_time.text!{
            showToast(controller: self, message: "End Date should be greater then Start Date.", seconds: 1.5)
            return
        }
        
        if txt_to_date_and_time.text! ==  txt_from_date_time.text!{
            showToast(controller: self, message: "End Date should be greater then Start Date.", seconds: 1.5)
            return
        }

        
        if indexPath_val.count < 1{
            showToast(controller: self, message: "Please Select Vehicle", seconds: 0.6)
            return
        }
        if txt_to_date_and_time.text!.count < 1 || txt_from_date_time.text!.count < 1{
            showToast(controller: self, message: "Fields can't be Empty", seconds: 0.3)
            return
        }
        
        let to_date = GetToDate(date : txt_to_date_and_time.text!)
        let to_time = GetToTime(time : txt_to_date_and_time.text!)


        let from_date = GetFromDate(date: txt_from_date_time.text!)
        let from_time = GetFromTime(time: txt_from_date_time.text!)
        
        var vehicle_val = [String]()
        for index in indexPath_val{
            vehicle_val.append(device_id[index])
            
        }
        
//        if !view_date_and_time.isHidden{
//            UIView.animate(withDuration: 0.5, animations: {
//                self.SetOrigin()
//            })
//            view_date_and_time.isHidden = true
//        }
        btn_filter.setImage(UIImage(named: "filterdown"), for: .normal)

        UIView.animate(withDuration: 0.5, animations: {
            self.view_vehicle_report.frame.origin.y = self.header_view.frame.size.height + 30
            self.view_vehicle_report.isHidden = false
        })
        view_vehicle_report.isHidden = false
        guard let locationController = children.first as? VehicleDistanceReportVC else  {
            fatalError("Check storyboard for missing LocationTableViewController")
        }
        report_data_controller = locationController
    let group_hour = lbl_group_time.text
        report_data_controller?.CallDataFromServer(to_date: to_date, from_date: from_date, to_time: to_time, from_time: from_time, device_id: vehicle_val, group_hour: group_hour!)
        
    }
    
    @objc func updateKmsLabel(sender: UISlider!) {
        let value = Int(sender.value)
        DispatchQueue.main.async {
            self.lbl_group_time.text = "Grouping Hours: " + "\(value)"
            // print("Slider value = \(value)")
        }
    }
    
}
extension VehicleDistanceVC{
    func CallVehicleFromServer(){
       
        self.view.addSubview(alert_view)
        vehicle_data.removeAll()
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let urlString = domain_name + Vehicle_Edit + "user_name=" + user_name + "&hash_key=" + hash_key
        
        CallVehicleData(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                //self.tbl_reports.isHidden = false
                for name in data!{
                    let v_name = name as! Dictionary<String, Any>
                    self.vehicle_data.append(v_name["registration_no"] as! String)
                    self.device_id.append(v_name["device_id"] as! String)
                }
                self.filterdData = self.vehicle_data
                self.tbl_sort.delegate = self
                self.tbl_sort.dataSource = self
                self.tbl_sort.reloadData()
            }else{
                //showToast(controller: self, message: "Please Check Internet Connection.", seconds: 0.3)
                self.tbl_sort.isHidden = true
                print("ERROR FOUND")
            }
            
            if r_error != nil{
                //print("sdfsdlklk \(String(describing: r_error))")
            }
            self.alert_view.removeFromSuperview()
        })
        
        
    }
}

extension VehicleDistanceVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterdData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_sort.dequeueReusableCell(withIdentifier: "cell_vehicle", for: indexPath) as! VehicleCell
        cell.lbl_item_name.text = filterdData[indexPath.row]
        
        if indexPath_val.count > 0{
            for val  in indexPath_val{
                if indexPath.row == val{
                    cell.img_check.isHidden = false
                    //selected_values.append(self.data[indexPath.row])
                }
            }
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tbl_sort.cellForRow(at: indexPath) as! VehicleCell
        
        if cell.img_check.isHidden{
            cell.img_check.isHidden = false
            self.indexPath_val.append(indexPath.row)
            //selected_values.append(self.vehicle_data[indexPath.row])
            vehicle_type_values.append(self.vehicle_data[indexPath.row])
        }else{
            cell.img_check.isHidden = true
            remove_values.append(self.vehicle_data[indexPath.row])
            self.indexPath_val_remove.append(indexPath.row)
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_sort.frame.size.height / 8
    }
    
    
}
