//
//  GraphCell.swift
//  GeoTrack
//
//  Created by Georadius on 09/08/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class GraphCell: UICollectionViewCell {
    
    @IBOutlet weak var basic_view_bar_chart: BasicBarChart!
   // @IBOutlet weak var view_bar_chart: BeautifulBarChart!
    @IBOutlet weak var view_content: UIView!
    
    private let numEntry = 5
    var values = [0, 0, 0, 0, 0]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        view_content.layer.cornerRadius = view_content.frame.size.height / 18

    }
    
    func SetGraph(){
        let dataEntries = generateRandomDataEntries()
        basic_view_bar_chart.updateDataEntries(dataEntries: dataEntries, animated: false)
        
//        let timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: true) {[weak self] (timer) in
//            let dataEntries = self!.generateRandomDataEntries()
//            self!.basic_view_bar_chart.updateDataEntries(dataEntries: dataEntries, animated: true)
//        }
//        timer.fire()
            
    }

    func generateEmptyDataEntries() -> [DataEntry] {
        var result: [DataEntry] = []
        Array(0..<numEntry).forEach {_ in
            result.append(DataEntry(color: UIColor.clear, height: 0, textValue: "0", title: ""))
        }
        return result
    }
    
    func generateRandomDataEntries() -> [DataEntry] {
        
        let colors = [#colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1), Global_Yellow_Color, Stopped_Color, Unreach_Color, #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)]
       let names = ["MOVING", "IDLE", "STOP", "NO DATA", "All"]
        var result: [DataEntry] = []
        
        var limit : Float = 100.0
        
        if values[4] > 0{
            limit = Float(values[4])
        }
//
        for i in 0..<numEntry {
            let value = values[i]
            let height: Float = Float(value) / limit
            
            //let formatter = DateFormatter()
            //formatter.dateFormat = "d MMM"
          //  var date = Date()
            //date.addTimeInterval(TimeInterval(24*60*60*i))
            result.append(DataEntry(color: colors[i % colors.count], height: height, textValue: "\(value)", title: names[i]))
        }
        return result
    }
    
}

