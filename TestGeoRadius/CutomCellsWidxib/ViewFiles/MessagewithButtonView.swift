//
//  MessagewithButtonView.swift
//  GeoTrack
//
//  Created by Georadius on 21/09/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class MessagewithButtonView: UIView {

    @IBOutlet weak var content_view: UIView!
    @IBOutlet weak var btn_no: UIButton!
    @IBOutlet weak var btn_yes: UIButton!
    
    
    override func draw(_ rect: CGRect) {
  
        content_view.layer.cornerRadius = content_view.frame.size.height / 18
        content_view.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        
    }
 
    
    class func instanceFromNib() -> MessagewithButtonView {
        let myClassNib = UINib(nibName: "MessagewithButtonView", bundle: nil)
        return myClassNib.instantiate(withOwner: nil, options: nil)[0] as! MessagewithButtonView
    }

}
