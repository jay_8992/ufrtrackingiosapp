//
//  UserAddList.swift
//  GeoTrack
//
//  Created by Georadius on 17/10/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class UserAddList: UIViewController {

    @IBOutlet weak var btn_add_user: UIButton!
    @IBOutlet weak var tbl_user_add: UITableView!
    @IBOutlet weak var view_header: UIView!
    
     var user_list : [UserListModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
           self.tbl_user_add.register(UINib(nibName: "UserAddCell", bundle: nil), forCellReuseIdentifier: "UserAddCell")
        btn_add_user.layer.cornerRadius = btn_add_user.frame.size.width / 2
        btn_add_user.addTarget(self, action: #selector(pressed_add_user), for: .touchUpInside)
        tbl_user_add.delegate = self
        tbl_user_add.dataSource = self
        
        
        CallUserList()
           
    }
    

    func CallUserList(){
        //https://track.gpsplatform.in/user_result.php?&action=search_user_json&user_name=premiumtech17@gmail.com&hash_key=VZPEKZMD&group_id=5601
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
              let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
              let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let group_id = UserDefaults.standard.value(forKey: GROUP_ID) as! String
        
        let urlString = domain_name + SEARCH_USER_JSON + K_USER_NAME + user_name + K_HASH_KEY + hash_key + K_GROUP_ID + group_id
      print("dfklkd \(urlString)")
        CallUserAddListFromServer(urlStirng: urlString, completionHandler: {data, r_error, isNetwork in
                if isNetwork && data != nil{
                    for val in data!{
                        let val_data = val as! Dictionary<String, Any>
                        let user_name = val_data["username"] as! String
                        let email = val_data["email"] as! String
                        let phone = val_data["phone"] as! String
                        let user_id = val_data["userid"] as! String
                      
                        let newDetail = UserListModel(user_name: user_name, phone: phone, email: email, user_id: user_id)
                        self.user_list.append(newDetail)
                        

                    }
                    self.tbl_user_add.isHidden = false
                    self.tbl_user_add.delegate = self
                    self.tbl_user_add.dataSource = self
                    self.tbl_user_add.reloadData()
                    
                }else{
                  //  self.tbl_invoice.isHidden = true
                    print("ERROR FOUND")
                }
                if r_error != nil{
                    showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                }
                
               // self.alert_view.removeFromSuperview()
            })
        
    }

    @objc func pressed_add_user(){
        let vc = self.storyboard?.instantiateViewController(identifier: "UserAddVC") as! UserAddVC
                   self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension UserAddList : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return user_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_user_add.dequeueReusableCell(withIdentifier: "UserAddCell") as! UserAddCell
        cell.lbl_user_name.text = user_list[indexPath.row].user_name
        cell.lbl_phone.text = user_list[indexPath.row].phone
        cell.lbl_email.text = user_list[indexPath.row].email
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_user_add.frame.size.height / 4.5
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, view, handler) in
            let vc = self.storyboard?.instantiateViewController(identifier: "DeleteUserVC") as! DeleteUserVC
             vc.user_id = self.user_list[indexPath.row].user_id
            self.navigationController?.pushViewController(vc, animated: true)
           }
        
        let editAction = UIContextualAction(style: .destructive, title: "Edit") { (action, view, handler) in
            let vc = self.storyboard?.instantiateViewController(identifier: "EditUserInfoVC") as! EditUserInfoVC
            self.navigationController?.pushViewController(vc, animated: true)
                  }
        
        let assignAction = UIContextualAction(style: .destructive, title: "Assign") { (action, view, handler) in
            let vc = self.storyboard?.instantiateViewController(identifier: "VehicleAssignment") as! VehicleAssignment
            self.navigationController?.pushViewController(vc, animated: true)
                  }
        
        let passwordAction = UIContextualAction(style: .destructive, title: "Password") { (action, view, handler) in
            let vc = self.storyboard?.instantiateViewController(identifier: "ResetPassword") as! ResetPassword
            vc.user_id = self.user_list[indexPath.row].user_id
            self.navigationController?.pushViewController(vc, animated: true)
                  }
        
        
       // deleteAction.image = UIImage(named: "cross")
           deleteAction.backgroundColor = .systemRed
        
       // editAction.image = UIImage(named: "cross")
                 editAction.backgroundColor = .systemYellow
        
        //assignAction.image = UIImage(named: "cross")
                 assignAction.backgroundColor = .systemGreen
        
      //  passwordAction.image = UIImage(named: "cross")
                 passwordAction.backgroundColor = .systemBlue
        
           let configuration = UISwipeActionsConfiguration(actions: [deleteAction, editAction, assignAction, passwordAction])
           configuration.performsFirstActionWithFullSwipe = false
           return configuration
    }
    
   
    
}
