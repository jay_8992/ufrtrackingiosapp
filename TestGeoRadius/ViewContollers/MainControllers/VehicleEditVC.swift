//
//  VehicleEditVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 09/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class VehicleEditVC: UIViewController {
    
    @IBOutlet weak var header_view: UIView!
    @IBOutlet weak var search_items: UISearchBar!
    @IBOutlet weak var tbl_show_items: UITableView!
    @IBOutlet weak var txt_select_vehicle: UITextField!
    @IBOutlet weak var txt_device_serial: UITextField!
    @IBOutlet weak var txt_voice: UITextField!
    @IBOutlet weak var txt_device_tag: UITextField!
    @IBOutlet weak var txt_vehicle_type: UITextField!
    @IBOutlet weak var txt_device_type: UITextField!
    @IBOutlet weak var txt_registration: UITextField!
      var searchActive : Bool = false
    @IBOutlet weak var btn_update: UIButton!
    var data = [String]()
    
    var filterd_device_tag = [String]()
    // self.device_tag.append(GetDeviceTag(Vehicals: v_name))
    
    var filterd_device_derial = [String]()
    //self.device_serial.append(GetDeviceSerial(Vehicals: v_name))
    var filterd_voice_no = [String]()
    
    //self.voice_no.append(GetVoiceNumber(Vehicals: v_name))
    
    var filterd_vehicle_type = [String]()
    // self.vehicle_type.append(GetVehicleTypeName(Vehicals: v_name))
    
    var filterd_vehicle_type_id = [String]()
    //self.vehicle_type_id.append(GetVehicleTypeID(Vehicals: v_name))
    // self.device_ids.append(GetDeviceID(Vehicals: v_name))
    var filterd_device_ids = [String]()
    
    var device_serial = [String]()
    var voice_no = [String]()
    var device_tag = [String]()
    var filterdData : [String]!
    var select_vehicle : String!
    var vehicle_type = [String]()
    var device_ids = [String]()
    var vehicle_type_id = [String]()
    var vehicle_typeid : String!
    var device_id : String!
    var isSelect_Vehicle = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        header_view.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        
        SetTexttFields()
        // Do any additional setup after loading the view.
        txt_voice.isEnabled = false
        //txt_device_tag.isEnabled = false
        txt_device_type.isEnabled = false
        //txt_registration.isEnabled = false
        txt_vehicle_type.isEnabled = false
        txt_device_serial.isEnabled = false
        
        let searchBarStyle = search_items.value(forKey: "searchField") as? UITextField
               searchBarStyle?.clearButtonMode = .never
        
        search_items.placeholder = "Search"
        self.tbl_show_items.register(UINib(nibName: "VehicleCell", bundle: nil), forCellReuseIdentifier: "cell_vehicle")
        txt_select_vehicle.addTarget(self, action: #selector(tap_select_vehicle), for: .editingDidBegin)
        txt_vehicle_type.addTarget(self, action: #selector(tap_vehicle_type), for: .editingDidBegin)
        btn_update.addTarget(self, action: #selector(submit_pressed), for: .touchUpInside)
        
    }
    
    @objc func tap_select_vehicle(){
        txt_select_vehicle.resignFirstResponder()
    }
    
    @objc func tap_vehicle_type(){
        txt_vehicle_type.resignFirstResponder()
    }
    
    @objc func submit_pressed(){
        
        if txt_registration.text!.count < 1 || txt_device_serial.text!.count < 1 || txt_voice.text!.count < 1{
            showToast(controller: self, message : "Please check all fields", seconds: 2.0)
            return
        }
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            // self.tbl_sort.isHidden = true
            // self.alert_view.removeFromSuperview()
            return
        }
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        
        
        let urlString = Update_Vehicle + txt_device_serial.text! + "&device_id=" + self.device_id! + "&vehicle_type=" + self.vehicle_typeid! + "&registration_no=" + txt_registration.text! + "&tag=" + txt_device_tag.text! + "&user_name=" + user_name + "&hash_key=" + hash_key
        
        let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: " ").inverted)
        
        
        CallUpdateDataOnServer(urlString: encodedString!, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                // print("kjkjkjk \(data!)")
                showToast(controller: self, message : data!, seconds: 2.0)
            }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                //print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
            
        })
    }
    
    func SetTexttFields(){
        SetTextFieldRightSide(imageName: "downarrow", txt_field: txt_select_vehicle)
        SetTextFieldRightSide(imageName: "downarrow", txt_field: txt_device_type)
        SetTextFieldRightSide(imageName: "downarrow", txt_field: txt_vehicle_type)
    }
    
    @IBAction func select_vhicle_tap(_ sender: Any) {
        CallVehicleFromServer()
        tbl_show_items.isHidden = false
        isSelect_Vehicle = true
        self.txt_vehicle_type.isEnabled = true
        
        UIView.animate(withDuration: 0.8, animations: {
            self.tbl_show_items.frame.origin.y = 20
        })
    }
    
    @IBAction func select_vehicle_type(_ sender: Any) {
        tbl_show_items.isHidden = false
        isSelect_Vehicle = false
        CallVehicleType()
        UIView.animate(withDuration: 0.8, animations: {
            self.tbl_show_items.frame.origin.y = 20
        })
    }
    
    func CallVehicleType(){
        data.removeAll()
        self.vehicle_type_id.removeAll()
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let vehicle_type_url = domain_name + Vehicle_Type + "user_name=" + user_name + "&hash_key=" + hash_key
        
        // print("slkslwjwhjh \(vehicle_type_url)")
        CallTrackResult(urlString: vehicle_type_url, completionHandler: {data, r_error, isNetwork in
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Array<Any>
                    for val in c_data{
                        let v_val = val as! Dictionary<String, Any>
                        self.data.append(v_val["vehicle_type_name"] as! String)
                        self.vehicle_type_id.append(GetVehicleTypeId(Vehicals: v_val))
                    }
                    
                    self.filterdData = self.data
                    self.filterd_vehicle_type_id = self.vehicle_type_id
                    self.tbl_show_items.reloadData()
                    //self.search_items.delegate = self
                    
                    break
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
                
            }else{
                print("ERROR FOUND")
            }
            
        })
    }
    
    func CallVehicleFromServer(){
        data.removeAll()
        device_tag.removeAll()
        self.device_serial.removeAll()
        self.voice_no.removeAll()
        self.vehicle_type.removeAll()
        self.device_ids.removeAll()
        self.vehicle_type_id.removeAll()
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let urlString = domain_name + Vehicle_Edit + "user_name=" + user_name + "&hash_key=" + hash_key
        
        // print("skseekj \(urlString)")
        
        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String,Any>
                    let val = c_data["return_json"] as! Array<Any>
                    
                    for name in val{
                        let v_name = name as! Dictionary<String, Any>
                        self.data.append(GetRegistrationNumber(Vehicals: v_name))
                        self.device_tag.append(GetDeviceTag(Vehicals: v_name))
                        self.device_serial.append(GetDeviceSerial(Vehicals: v_name))
                        self.voice_no.append(GetVoiceNumber(Vehicals: v_name))
                        self.vehicle_type.append(GetVehicleTypeName(Vehicals: v_name))
                        self.vehicle_type_id.append(GetVehicleTypeID(Vehicals: v_name))
                        self.device_ids.append(GetDeviceID(Vehicals: v_name))
                    }
                    if self.isSelect_Vehicle{
                        self.filterdData = self.data
                        self.filterd_voice_no = self.voice_no
                        self.filterd_device_tag = self.device_tag
                        self.filterd_device_derial = self.device_serial
                        self.filterd_vehicle_type = self.vehicle_type
                        self.filterd_vehicle_type_id = self.vehicle_type_id
                        self.filterd_device_ids = self.device_ids
                        
                        self.tbl_show_items.delegate = self
                        self.tbl_show_items.dataSource = self
                        self.tbl_show_items.reloadData()
                        self.search_items.delegate = self
                    }
                    
                    break
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
                
            }else{
                print("ERROR FOUND")
            }
            
            
        })
    }
    
    
}

extension VehicleEditVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterdData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_show_items.dequeueReusableCell(withIdentifier: "cell_vehicle") as! VehicleCell
        cell.lbl_item_name.text = filterdData[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_show_items.frame.size.height / 8
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.8, animations: {
            self.tbl_show_items.frame.origin.y = self.tbl_show_items.frame.size.height
            //self.tbl_show_items.isHidden = true
            if self.isSelect_Vehicle{
                self.txt_select_vehicle.text = self.filterdData[indexPath.row]
                self.txt_device_serial.text = self.filterd_device_derial[indexPath.row]
                self.txt_voice.text = self.filterd_voice_no[indexPath.row]
                self.txt_device_tag.text = self.filterd_device_tag[indexPath.row]
                self.txt_registration.text = self.filterdData[indexPath.row]
                self.txt_vehicle_type.text = self.filterd_vehicle_type[indexPath.row]
                self.device_id = self.filterd_device_ids[indexPath.row]
                self.vehicle_typeid = self.filterd_vehicle_type_id[indexPath.row]
            }else{
               
                self.txt_vehicle_type.text = self.filterdData[indexPath.row]
                
                self.vehicle_typeid = self.filterd_vehicle_type_id[indexPath.row]
            }
            
        })
        self.search_items.endEditing(true)
    }
}

extension VehicleEditVC : UISearchBarDelegate{
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//
//        // filterdata  = searchText.isEmpty ? data : data.filter {(item : String) -> Bool in
//
//        filterdData = searchText.isEmpty ? data : data.filter { $0.contains(searchText) }
//
//        //return item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
//
//        tbl_show_items.reloadData()
//    }
//
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        filterdData = data
//        tbl_show_items.reloadData()
//    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
          searchActive = true;
      }
      
      func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
          searchActive = false;
          self.search_items.endEditing(true)
          self.tbl_show_items.reloadData()
      }
      
      func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
          searchActive = false;
          self.search_items.endEditing(true)
      }
      
      func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
          searchActive = false;
          self.search_items.endEditing(true)
      }
      
      func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
          
        filterdData = searchText.isEmpty ? data : data.filter { $0.contains(searchText) }
          if(filterdData.count == 0){
              searchActive = false;
          } else {
              searchActive = true;
          }
        
          self.tbl_show_items.reloadData()
      }
}

