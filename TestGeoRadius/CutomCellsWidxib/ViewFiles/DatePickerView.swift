//
//  DatePickerView.swift
//  GeoTrack
//
//  Created by Georadius on 31/07/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class DatePickerView: UIView {

    @IBOutlet weak var btn_cancel: UIButton!
    @IBOutlet weak var date_view_picker: UIDatePicker!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    class func instanceFromNib() -> DatePickerView {
        let myClassNib = UINib(nibName: "DatePickerView", bundle: nil)
        return myClassNib.instantiate(withOwner: nil, options: nil)[0] as! DatePickerView
    }

}
