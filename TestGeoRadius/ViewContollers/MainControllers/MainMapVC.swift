//
//  MainMapVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 08/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps
import Mapbox

class MyCustomPointAnnotation: MGLPointAnnotation {
    var willUseImage: Bool = false
}

class MainMapVC: UIViewController, MGLMapViewDelegate, GMUClusterManagerDelegate, GMSMapViewDelegate, GMUClusterRendererDelegate{


    @IBOutlet weak var btn_zoom_out: UIButton!
    @IBOutlet weak var btn_zoom_in: UIButton!
    @IBOutlet weak var btn_traffic: UIButton!
    @IBOutlet weak var map_show: UIView!
    @IBOutlet weak var map_view: MGLMapView!
    @IBOutlet weak var map_apple: MKMapView!
    @IBOutlet weak var showMap: GMSMapView!
    @IBOutlet weak var btn_google_map: UIButton!
    @IBOutlet weak var btn_apple_map: UIButton!
    
    var mapView: MGLMapView!
    var vehicals : Array<Any>?
    var latitude = [Double]()
    var registration = [String]()
    var vehicle_type_id = [String]()
    var longitude = [Double]()
    var direction = [Double]()
    var device_id = [String]()
    var rasterLayer: MGLRasterStyleLayer?
    var count = 0
    var imageName : String = STOP_CAR
    var status = [Int]()
    var clusterManager: GMUClusterManager!
    var moving = false
    var idle = false
    var stopped = false
    var no_data = false
    var all = true
    var is_queue = false
    let alert_view = AlertView.instanceFromNib()
    var vehicals_s : Array<Any>?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        mapView = MGLMapView(frame: view.bounds, styleURL: MGLStyle.lightStyleURL)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.tintColor = .darkGray
        map_show.addSubview(mapView)
        //mapView.delegate = self
      
        btn_traffic.layer.cornerRadius = btn_traffic.frame.size.height / 10
        btn_traffic.layer.borderColor = UIColor.darkGray.cgColor
        btn_traffic.layer.borderWidth = 1
        
        btn_zoom_in.layer.cornerRadius = btn_zoom_in.frame.size.width / 2
        btn_zoom_in.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_zoom_in.addTarget(self, action: #selector(pressed_zoom_in), for: .touchUpInside)
        
        btn_zoom_out.layer.cornerRadius = btn_zoom_out.frame.size.width / 2
        btn_zoom_out.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_zoom_out.addTarget(self, action: #selector(pressed_zoom_out), for: .touchUpInside)
       
        
        mapView.setCenter(CLLocationCoordinate2D(latitude: 28.6127, longitude: 77.2773), zoomLevel: 10, animated: false)
        let point = MyCustomPointAnnotation()
        point.coordinate = CLLocationCoordinate2D(latitude: 28.6127, longitude: 77.2773)
        
        let myPlaces = [point]
        mapView.removeAnnotations(myPlaces)
        mapView.addAnnotations(myPlaces)
        
        // -------------------------------------SetGoogleMap-----------------------------------//
        
        self.showMap.animate(toLocation: CLLocationCoordinate2D(latitude: 28.6127, longitude: 77.2773))
        let camera = GMSCameraPosition.camera(withLatitude: 28.6127, longitude: 77.2773, zoom: 10)
        self.showMap!.camera = camera

    }

    
    override func viewWillAppear(_ animated: Bool) {
        let is_global_selection = UserDefaults.standard.value(forKey: "is_global_selection") as? Bool ?? false
        print(is_global_selection)
        if is_global_selection{
            CallTrakingDataFromServer()
            SetDataOnMAp()
        }
    }
    
    
    func CheckIsHidden(isHidden_V: Bool){
        if isHidden_V{
            is_queue = false
            StopAllThreads()
        }
    }
    
    func Check(Moving: Bool, Idle: Bool, Stopped: Bool, No_data: Bool, All: Bool, select: Bool, vehicle_data: Array<Any>){
        if vehicle_data.count < 1{
            showMap.isHidden = true
            return
        }
        showMap.isHidden = false
        alert_view.frame.size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(self.alert_view)
        self.vehicals = vehicle_data
        self.vehicals_s = vehicle_data
        self.latitude.removeAll()
        self.longitude.removeAll()
        self.status.removeAll()
        self.registration.removeAll()
        self.device_id.removeAll()
       
        UIView.animate(withDuration: 0.0, animations: {
            self.SetDataOnMAp()
            
        }, completion: { _ in
          
            self.moving = Moving
            self.idle = Idle
            self.stopped = Stopped
            self.no_data = No_data
            self.all = All
            
            for (_, loc) in vehicle_data.enumerated(){
                let val_data = loc as! Dictionary<String, Any>
                let status = val_data["device_status"] as! Int
                if self.moving{
                    if status == 2{
                        self.SetAllData(val_data: val_data)
                    }
                }else if self.idle{
                    if status == 1{
                        self.SetAllData(val_data: val_data)
                    }
                }else if self.stopped{
                    if status == 0{
                        self.SetAllData(val_data: val_data)
                    }
                    
                }else if self.no_data{
                    if status == 4{
                        //if index < 200{
                            self.SetAllData(val_data: val_data)
                       
                    }
                }else if self.all{
            //if index < 200{
                       self.SetAllData(val_data: val_data)
                    
                }
            }
        })
    }
    
    
    @IBAction func btn_traffic_pressed(_ sender: Any) {
        if showMap.isTrafficEnabled{
            showMap.isTrafficEnabled = false
            btn_traffic.setImage(UIImage(named: "no_traffic"), for: .normal)
        }else{
            showMap.isTrafficEnabled = true
             btn_traffic.setImage(UIImage(named: "traffic"), for: .normal)
        }
    }
    
    @IBAction func select_map_type(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            showMap.mapType = .normal
            
        case 1:
            showMap.mapType = .satellite
           
        default:
            showMap.mapType = .hybrid
        }
    }
    
    
    func SetAllData(val_data: Dictionary<String, Any>){
        
        self.latitude.append(GetLatitudeFromData(Vehicals : val_data))
        self.longitude.append(GetLonitudeFromData(Vehicals: val_data))
        status.append(GetDeviceStatus(Vehicals: val_data))
        vehicle_type_id.append(GetVehicleTypeId(Vehicals: val_data))
        registration.append(GetRegistrationNumber(Vehicals: val_data))
        device_id.append(GetDeviceID(Vehicals: val_data))
        let angle = Double(val_data["direction"] as! String)
        self.direction.append(angle!)
        
         UIView.animate(withDuration: 0.0, animations: {
            let iconGenerator = GMUDefaultClusterIconGenerator()
            let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
            let renderer = GMUDefaultClusterRenderer(mapView: self.showMap,
                                                     clusterIconGenerator: iconGenerator)
            self.clusterManager = GMUClusterManager(map: self.showMap, algorithm: algorithm,
                                               renderer: renderer)
            // Generate and add random items to the cluster manager.
            renderer.delegate = self
            renderer.update()
            self.showMap.delegate = self
            self.generateClusterItems()
            // Call cluster() after items have been added to perform the clustering
            // and rendering on map.
            self.clusterManager.cluster()
             //self.mapView.delegate = self
            self.clusterManager.setDelegate(self, mapDelegate: self)
           // self.SetDataOnMAp()
        }, completion: { _ in
            self.alert_view.removeFromSuperview()
        })
    }
    
    
    
    func CallTrakingDataFromServer(){

        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.showMap.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        alert_view.frame.size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(self.alert_view)

        
        vehicals?.removeAll()
        latitude.removeAll()
        longitude.removeAll()
        status.removeAll()
        registration.removeAll()
        device_id.removeAll()
        vehicals_s?.removeAll()
        direction.removeAll()
        vehicle_type_id.removeAll()
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let urlString = domain_name + Track_Base + "user_name=" + user_name + "&hash_key=" + hash_key
        
        
        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
                let result = data?[K_Result] as! Int
                switch (result){
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String,Any>
                    self.vehicals = (c_data[VEHICALS] as! Array<Any>)
                    self.vehicals_s = (c_data[VEHICALS] as! Array<Any>)
                    for loc in self.vehicals!{
                        let val_data = loc as! Dictionary<String, Any>
                        let status = val_data["device_status"] as! Int
                        if self.moving{
                            if status == 2{
                                self.SetAllData(val_data: val_data)
                            }
                        }else if self.idle{
                            if status == 1{
                                self.SetAllData(val_data: val_data)
                            }
                        }else if self.stopped{
                            if status == 0{
                                self.SetAllData(val_data: val_data)
                            }
                        } else if self.no_data{
                            if status == 4{
                                self.SetAllData(val_data: val_data)
                            }
                        }else{
                            self.SetAllData(val_data: val_data)
                        }
                    }
                    break
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
            }else{
                //showToast(controller: self, message: "Please check your Internet Connection.", seconds: 0.3)
                print("ERROR FOUND")
            }
            if self.is_queue{
            DispatchQueue.main.asyncAfter(deadline: .now() + 50.0) {
                // print("Timer fired! ---------")
                [weak self] in
                if self?.is_queue != nil && (self?.is_queue)! {
                self!.CallTrakingDataFromServer()
                }
             }
        }
            self.alert_view.removeFromSuperview()
        })
    }
    
    
    @objc func GetTrackingData(notification: Notification){
        let c_data = notification.userInfo
        vehicals?.removeAll()
        latitude.removeAll()
        longitude.removeAll()
        status.removeAll()
        registration.removeAll()
        device_id.removeAll()
        vehicals = (c_data![VEHICALS] as! Array<Any>)
        for loc in vehicals! {
            let val_data = loc as! Dictionary<String, Any>
             let status = val_data["device_status"] as! Int
            if moving{
                if status == 2{
                    SetAllData(val_data: val_data)
                }
            } else if idle {
                if status == 1{
                    SetAllData(val_data: val_data)
                }
            } else if stopped {
                if status == 0{
                    SetAllData(val_data: val_data)
                }
            } else if no_data {
                if status == 4{
                    SetAllData(val_data: val_data)
                }
            } else{
                SetAllData(val_data: val_data)
            }
        }
    }
    

    
    @objc func pressed_zoom_in(){
        
        let loc : CLLocation = CLLocation(latitude: latitude[0], longitude: longitude[0])
        let zoom_in = self.showMap.camera.zoom + 2
        let camera = GMSCameraPosition.camera(withTarget: loc.coordinate, zoom: zoom_in)
        self.showMap!.camera = camera
    
    }
    
    @objc func pressed_zoom_out(){
        let loc : CLLocation = CLLocation(latitude: latitude[0], longitude: longitude[0])
        let zoom_in = self.showMap.camera.zoom - 2
        let camera = GMSCameraPosition.camera(withTarget: loc.coordinate, zoom: zoom_in)
        self.showMap!.camera = camera
    }
    
    @objc func showGooleMap(){
        showMap.isHidden = false
        map_apple.isHidden = true
    }
  
    @objc func showAppleMap(){
        showMap.isHidden = true
        map_apple.isHidden = false
    }
    
    
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
        let source = MGLRasterTileSource(identifier: IDENTIFIER, tileURLTemplates: [Title_URL], options: [ .tileSize: 256 ])
        let rasterLayer = MGLRasterStyleLayer(identifier: IDENTIFIER, source: source)
        style.addSource(source)
        style.addLayer(rasterLayer)
        self.rasterLayer = rasterLayer
    }

    
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        guard annotation is MGLPointAnnotation else {
            return nil
        }
        
        let reuseIdentifier = imageName
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        if annotationView == nil {
            annotationView = CustomImageAnnotationView(reuseIdentifier: reuseIdentifier, image: UIImage(named: imageName)!)
        }
       
        return annotationView
    }

    
    func mapView(_ mapView: MGLMapView, imageFor annotation: MGLAnnotation) -> MGLAnnotationImage? {
        if let castAnnotation = annotation as? MyCustomPointAnnotation {
            if (!castAnnotation.willUseImage) {
                return nil
            }
        }
        
        var annotationImage = mapView.dequeueReusableAnnotationImage(withIdentifier: imageName)
        if(annotationImage == nil) {
            annotationImage = MGLAnnotationImage(image: UIImage(named: imageName)!, reuseIdentifier: imageName)
        }
        
        return annotationImage
  }
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }

    func mapView(_ mapView: MGLMapView, tapOnCalloutFor annotation: MGLAnnotation) {
        //print("tap on callout \(annotation.hash)")
    }
    
    func mapView(_ mapView: MGLMapView, didSelect annotationView: MGLAnnotationView) {
       // print("kjkjjkjkjkjkj")
    }

}

extension MainMapVC{
    
    func SetDataOnMAp(){
        for (index, _) in self.registration.enumerated(){
                let point = MyCustomPointAnnotation()
                point.coordinate = CLLocationCoordinate2D(latitude: self.latitude[index], longitude: self.longitude[index])
            
            var vehicle_name = "car"
            if vehicle_type_id[index] == "56"{
                vehicle_name = "police"
            }else if vehicle_type_id[index] == "1"{
                vehicle_name = "car"
            }else if vehicle_type_id[index] == "2"{
                vehicle_name = "truck"
            }else if vehicle_type_id[index] == "3"{
                vehicle_name = "bus"
            }else if vehicle_type_id[index] == "4"{
                vehicle_name = "van"
            }else if vehicle_type_id[index] == "5"{
                vehicle_name = "car"
            }else if vehicle_type_id[index] == "6"{
                vehicle_name = "bike"
            }else if vehicle_type_id[index] == "7"{
                vehicle_name = "scooty"
            }else{
                vehicle_name = "car"
            }
            
                if status[index] == 0{
                    imageName = vehicle_name + "_red"
                }
                if status[index] == 1{
                    imageName = vehicle_name + "_yellow"
                }
                if status[index] == 2{
                    imageName = vehicle_name + "_green"
                }
                if status[index] == 4{
                    imageName = vehicle_name + "_black"
                }
                
                point.title = self.registration[index]
                let myPlaces = [point]
                self.mapView.addAnnotations(myPlaces)
        }
    }
    
    // -------------------------------------------------- Google Map -----------------------------------//
    
    func SetGoogleDataOnMap(){
       
        self.showMap.animate(toLocation: CLLocationCoordinate2D(latitude: self.latitude[0], longitude: self.longitude[0]))
        let camera = GMSCameraPosition.camera(withLatitude: self.latitude[0], longitude: self.longitude[0], zoom: 10)
        self.showMap!.camera = camera
        
        
        var marker = GMSMarker()
        for (index, _) in self.registration.enumerated()
        {
            let initialLocation = CLLocationCoordinate2DMake(self.latitude[index], self.longitude[index])
            marker = GMSMarker(position: initialLocation)
            
            var vehicle_name = "car"
            if vehicle_type_id[index] == "56"{
                vehicle_name = "police"
            }else if vehicle_type_id[index] == "1"{
                vehicle_name = "car"
            }else if vehicle_type_id[index] == "2"{
                vehicle_name = "truck"
            }else if vehicle_type_id[index] == "3"{
                vehicle_name = "bus"
            }else if vehicle_type_id[index] == "4"{
                vehicle_name = "van"
            }else if vehicle_type_id[index] == "5"{
                vehicle_name = "car"
            }else if vehicle_type_id[index] == "6"{
                vehicle_name = "bike"
            }else if vehicle_type_id[index] == "7"{
                vehicle_name = "scooty"
            }else{
                vehicle_name = "car"
            }
            
            if status[index] == 0{
                imageName = vehicle_name + "_red"
            }
            if status[index] == 1{
                imageName = vehicle_name + "_yellow"
            }
            if status[index] == 2{
                imageName = vehicle_name + "_green"
            }
            if status[index] == 4{
                imageName = vehicle_name + "_black"
            }
            
            marker.icon = UIImage(named: imageName)
            marker.map = self.showMap
        }
    }
    
    func generateClusterItems() {
       
        for index in 0...self.latitude.count - 1 {
            let lat = self.latitude[index]
            let lng = self.longitude[index]
            let name = self.registration[index]
            let item =
                POIItem(position: CLLocationCoordinate2DMake(lat, lng), name: name)
            
            clusterManager.add(item)
        }
    }
    
    func randomScale() -> Double {
        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
    }
    
    // MARK: - GMUClusterManagerDelegate
    
    func clusterManager(clusterManager: GMUClusterManager, didTapCluster cluster: GMUCluster) {
        
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position, zoom: showMap.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        showMap.moveCamera(update)
    }
    
    
    // MARK: - GMUMapViewDelegate
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let poiItem = marker.userData as? POIItem {
            marker.title = poiItem.name!
            
        } else {
            NSLog("Did tap a normal marker")
        }
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        self.is_queue = false
        var index_val : Int!
        var deviceID = [String]()
        for (index, loc) in (vehicals_s?.enumerated())!{
             let val_data = loc as! Dictionary<String, Any>
              deviceID.append(GetDeviceID(Vehicals: val_data))
            if marker.title == GetRegistrationNumber(Vehicals: val_data){
                 index_val = index
            }
        }
        
        UserDefaults.standard.set(false, forKey: "is_global_selection")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShowTrackingInfo") as! ShowTrackingInfo
        vc.index = index_val
        vc.vehicals = vehicals_s
        vc.device_id = deviceID[index_val]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
   
    
    func renderer(_ renderer: GMUClusterRenderer, didRenderMarker marker: GMSMarker) {
   
        var image_name = STOP_CAR
        var direction : Double = 0.0
        
        for (index, _) in self.latitude.enumerated(){
            if marker.position.latitude == latitude[index]{
                var vehicle_name = "car"
                if vehicle_type_id[index] == "56"{
                    vehicle_name = "police"
                }else if vehicle_type_id[index] == "1"{
                    vehicle_name = "car"
                }else if vehicle_type_id[index] == "2"{
                    vehicle_name = "truck"
                }else if vehicle_type_id[index] == "3"{
                    vehicle_name = "bus"
                }else if vehicle_type_id[index] == "4"{
                    vehicle_name = "van"
                }else if vehicle_type_id[index] == "5"{
                    vehicle_name = "car"
                }else if vehicle_type_id[index] == "6"{
                    vehicle_name = "bike"
                }else if vehicle_type_id[index] == "7"{
                    vehicle_name = "scooty"
                }else{
                    vehicle_name = "car"
                }
                
                
                if status[index] == 0{
                    image_name = vehicle_name + "_red"
                    direction = self.direction[index]
                }
                if status[index] == 1{
                    direction = self.direction[index]
                    image_name = vehicle_name + "_yellow"
                }
                if status[index] == 2{
                    direction = self.direction[index]
                    image_name = vehicle_name + "_green"
                }
                if status[index] == 4{
                    direction = self.direction[index]
                    image_name = vehicle_name + "_black"
                }
                
 
            }
            marker.groundAnchor = CGPoint(x: 0.5, y: 1)
            if (marker.userData as? POIItem) != nil {
                let vehicle = UIImage(named: image_name)!
                let markerView = UIImageView(image: vehicle)
                marker.iconView = markerView
                 marker.rotation = direction
            }
        }
        
    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let camera = GMSCameraPosition.camera(withLatitude: latitude[0], longitude: longitude[0], zoom: 12)
        showMap.animate(to: camera)
        //self.showMap!.camera = camera
        return true
    }
    
  
    
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
//
//        print("ddkfdkfdlk \(marker.position)")
//        marker.groundAnchor = CGPoint(x: 0.5, y: 1)
//
//        if (marker.userData as? POIItem) != nil {
//
//             let vehicle = UIImage(named: imageName)!
//            let markerView = UIImageView(image: vehicle)
//            marker.iconView = markerView
//        }
    }
    
}

class POIItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var name: String?
  
    
    init(position: CLLocationCoordinate2D, name: String?) {
        self.position = position
        self.name = name
       
    }
}
