//
//  SideBarCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 27/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class SideBarCell: UITableViewCell {

    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_contact: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
