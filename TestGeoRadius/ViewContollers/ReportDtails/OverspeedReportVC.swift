//
//  OverspeedReportVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 21/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class OverspeedReportVC: UIViewController {

    @IBOutlet weak var lbl_error: UILabel!
    @IBOutlet weak var tbl_report: UITableView!
    let alert_view = AlertView.instanceFromNib()
    
    var registration_no = [String]()
    
    var overspeed_data = [NSArray]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tbl_report.register(UINib(nibName: "OverSpeedReportCell", bundle: nil), forCellReuseIdentifier: "OverSpeedReportCell")
        self.tbl_report.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier: "NoDataCell")

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        //CallDataFromServer()
        
        let buttonTop = UIButton(frame: CGRect(x: self.view.frame.size.width - 70, y: self.view.frame.size.height - 100, width: 50, height: 50))
        buttonTop.backgroundColor = UIColor.darkGray
        buttonTop.layer.cornerRadius = buttonTop.frame.size.width / 2
        buttonTop.setImage(UIImage(named: "up"), for: .normal)
        buttonTop.addTarget(self, action: #selector(BottomToTop), for: .touchUpInside)
        self.view.addSubview(buttonTop)
        
    }
    
    
    @objc func BottomToTop(){
         if registration_no.count > 0{
        let indexPath = IndexPath(row: 0, section: 0)
        self.tbl_report.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    func CallDataFromServer(to_date: String, from_date: String, to_time: String, from_time: String, device_id: [String], group_hour: String){
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        self.view.addSubview(alert_view)
        
        registration_no.removeAll()
        overspeed_data.removeAll()
        
        let device_ids = device_id.joined(separator: ",")
        //let alert_type_ids = alert_type_id.joined(separator: ",")
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let from_group_hour = group_hour
        let date_time = from_group_hour.components(separatedBy: " ")
        let group_h = date_time[1]
        
        let urlString = domain_name + "/report_over_speed_result.php?action=report_overspeed_json_new&device_id="  +  device_ids + "&date_from="  +  from_date + "&date_to="  + to_date  +  "&time_picker_from="  +  from_time  +  "&time_picker_to="  +  to_time  +  "&speed_limit="  +  group_h +  "&user_name=" + user_name + "&hash_key=" + hash_key
        
       // print("dkdkjl \(urlString)")
        
       // let dkfjdkj = domain_name + "/report_over_speed_result.php?action=report_overspeed_json&device_id=" + device_ids + "&date_from=" + from_date + "&date_to=" + to_date + "&time_picker_from=00:00:00&time_picker_to=23:59:59&speed_limit=40&user_name=amitabh&hash_key=LYIWMQWJ"
        
        CallOverSpeedReport(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                self.tbl_report.isHidden = false
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                    self.registration_no.append(GetRegistrationNumber(Vehicals: val_data))
                    self.overspeed_data.append(val_data["overspeed_data"] as! NSArray)
                }
                self.tbl_report.delegate = self
                self.tbl_report.dataSource = self
                self.tbl_report.reloadData()
            }else{
                
                if data != nil{
                    self.lbl_error.text = "Something went wrong."
                    self.tbl_report.isHidden = true
                }else{
                   // showToast(controller: self, message: "Please Check Your internet Connection.", seconds: 0.3)
                    self.tbl_report.isHidden = true
                }
                
              
                print("ERROR FOUND")
            }
            
            if r_error != nil{
                self.lbl_error.text = r_error
                self.tbl_report.isHidden = true
            }
            self.alert_view.removeFromSuperview()
        })
        
    }

}

extension OverspeedReportVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return registration_no.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if overspeed_data[section].count < 1{
            return 1
        }else{
            return overspeed_data[section].count

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_report.dequeueReusableCell(withIdentifier: "OverSpeedReportCell") as! OverSpeedReportCell
        let no_cell = tbl_report.dequeueReusableCell(withIdentifier: "NoDataCell") as! NoDataCell

        if overspeed_data[indexPath.section].count < 1{
            return no_cell
        }else{
            let vehicle_distance = overspeed_data[indexPath.section][indexPath.row] as! Dictionary<String, Any>
            //print("aslsklk \(vehicle_distance)")
            cell.lbl_speed.text = vehicle_distance["speed"] as? String
            //        cell.lbl_start.attributedText = FixBoldBetweenText(firstString: self.start[indexPath.row], boldFontName: " on ", lastString: self.start_date[indexPath.row])
            cell.lbl_location.attributedText = FixBoldBetweenText(firstString: vehicle_distance["location_name"] as! String, boldFontName: " on ", lastString: vehicle_distance["log_date_start"] as! String)
            
            return cell
        }
     
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return tbl_report.frame.size.height / 4
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 55
        
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = HeaderViewReportAll.instanceFromNib()
        view.lbl_registration.text = registration_no[section]
        return view
    }
    
    
}

