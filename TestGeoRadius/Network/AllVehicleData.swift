//
//  AllVehicleData.swift
//  GeoTrack
//
//  Created by Georadius on 15/10/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import Foundation

class AllVehicleData{
    
    var all_vehicle_data : Array<Any>!
    
  
    
    func Set_All_Vehicle_Data(all_vehicle_data : Array<Any>){
        self.all_vehicle_data = all_vehicle_data
    }
    
    func retrieveData() -> Array<Any>{
        return all_vehicle_data
    }
    
}
