//
//  Report_Billing_Cell.swift
//  TestGeoRadius
//
//  Created by Georadius on 09/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class Report_Billing_Cell: UITableViewCell {

    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var lbl_report_name: UILabel!
    @IBOutlet weak var img_report: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        view_back.layer.cornerRadius = view_back.frame.size.height / 18
        view_back.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
