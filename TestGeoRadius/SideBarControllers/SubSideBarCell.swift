//
//  SubSideBarCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 28/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class SubSideBarCell: UITableViewCell {

    @IBOutlet weak var img_item: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
