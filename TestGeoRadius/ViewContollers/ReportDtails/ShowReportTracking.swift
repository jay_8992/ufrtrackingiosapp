//
//  ShowReportTracking.swift
//  TestGeoRadius
//
//  Created by Georadius on 05/06/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class ShowReportTracking: UIViewController {
    let alert_view = AlertView.instanceFromNib()
    
    var avg_speed = [String]()
    var distance = [String]()
    var date_time = [String]()
    var start = [String]()
    var end = [String]()
    var start_date = [String]()
    var end_date = [String]()
    var vehical_status = [String]()
    
    var trip_detail_url : String?
    @IBOutlet weak var tbl_reports: UITableView!
    @IBOutlet weak var lbl_error: UILabel!
    var device_id : String?
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

     
        
        self.tbl_reports.register(UINib(nibName: "TripReportCell", bundle: nil), forCellReuseIdentifier: "TripReportCell")
        // Do any additional setup after loading the view.
        CallDataFromServer()
    }
    
    func CallDataFromServer(){
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.tbl_reports.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        //alert_data_registration.removeAll()
        self.view.addSubview(alert_view)
        
        avg_speed.removeAll()
        self.distance.removeAll()
        date_time.removeAll()
        start.removeAll()
        end.removeAll()
        start_date.removeAll()
        end_date.removeAll()
        vehical_status.removeAll()
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let from_date = GetFromDate(date: TodayFromDate())
        let to_date = GetToDate(date: TodayToDate())
        let from_time = GetFromTime(time: TodayFromDate())
        let to_time = GetToTime(time: TodayToDate())
        let distance = ""
    
        
        
        let urlString1 = domain_name + Trip_All + device_id! + "&date_from=" + from_date
    
        let urlString2 =  "&date_to=" + to_date + "&time_picker_from=" + from_time + "&time_picker_to=" + to_time + "&distance_filter="
        
        let urlString3 = distance + "&user_name=" + user_name + "&hash_key=" + hash_key + Trip_All_Last
        
        let urlString = urlString1 + urlString2 + urlString3
        
        
        
        
        let trip_detail_url1 = domain_name + Trip_Detail + device_id! + "&start_date="
          
         let trip_detail_url2 =  from_date + " " + from_time +  "&end_date=" + to_date
           
         let trip_detail_url3 = " " + to_time + "&user_name=" + user_name + "&hash_key=" + hash_key
            
        
        trip_detail_url = trip_detail_url1 + trip_detail_url2 + trip_detail_url3
        
        
        CallTripReport(urlString: urlString, key_val: "trip_data", completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                self.tbl_reports.isHidden = false
                
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                    self.date_time.append(val_data["time_interval"] as! String)
                    self.avg_speed.append(val_data["average_speed"] as! String)
                    let dist = String(val_data["distance"] as! String)
                    self.distance.append(dist)
                    self.start.append(val_data["start_location"] as! String)
                    self.end.append(val_data["end_location"] as! String)
                    self.start_date.append(val_data["start_date"] as! String)
                    self.end_date.append(val_data["end_date"] as! String)
                    //self.vehical_status.append(val_data["vehicle_status"] as! String)
                    
                    self.vehical_status.append(GetVehicleStatus(Vehicals: val_data))
                }
                self.tbl_reports.delegate = self
                self.tbl_reports.dataSource = self
                self.tbl_reports.reloadData()
            }else{
                if data != nil{
                    self.lbl_error.text = "Something went wrong."
                    self.tbl_reports.isHidden = true
                }else{
                    //showToast(controller: self, message: "Please check your Internet Connection.", seconds: 0.3)
                    self.tbl_reports.isHidden = true
                }
               
            }
            
            if r_error != nil{
                self.lbl_error.text = r_error
                self.tbl_reports.isHidden = true
            }
            self.alert_view.removeFromSuperview()
        })
    }
    

}

extension ShowReportTracking : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return date_time.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_reports.dequeueReusableCell(withIdentifier: "TripReportCell") as! TripReportCell
        cell.lbl_interval.text = date_time[indexPath.row]
        cell.lbl_avg_speed.text = avg_speed[indexPath.row] + " KMPH"
        cell.lbl_distance.text = distance[indexPath.row] + " Km"
        
        
        cell.lbl_start.attributedText = FixBoldBetweenText(firstString: self.start[indexPath.row], boldFontName: " on ", lastString: self.start_date[indexPath.row])
        
        
        cell.lbl_end.attributedText = FixBoldBetweenText(firstString: self.end[indexPath.row], boldFontName: " on ", lastString: self.end_date[indexPath.row])
        
        if vehical_status[indexPath.row] == "Moving"{
            cell.img_val.image = UIImage(named: "trip_green")
        }
        if vehical_status[indexPath.row] == "Idle"{
            cell.img_val.image = UIImage(named: "trip_yellow")
        }
        if vehical_status[indexPath.row] == "Stopped"{
            cell.img_val.image = UIImage(named: "trip_red")
        }
        if vehical_status[indexPath.row] == "Unreachable"{
            cell.img_val.image = UIImage(named: "trip_black")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_reports.frame.size.height / 1.8
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if vehical_status[indexPath.row] == "Moving"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TripDetailVC") as! TripDetailVC
            vc.urlString = trip_detail_url
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

