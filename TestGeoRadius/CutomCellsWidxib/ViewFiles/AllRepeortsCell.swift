//
//  AllRepeortsCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 17/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class AllRepeortsCell: UITableViewCell {

    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var lbl_time: UILabel!
    @IBOutlet weak var lbl_location: UILabel!
    @IBOutlet weak var lbl_alert: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        view_content.layer.cornerRadius = view_content.frame.size.height / 18
        view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
