//
//  Extensions.swift
//  TestGeoRadius
//
//  Created by Georadius on 18/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import Foundation
import UIKit


class Colors {
    var gl:CAGradientLayer!

    init() {
        let colorTop = UIColor(red: 156.0 / 255.0, green: 103.0 / 255.0, blue: 183.0 / 255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 70.0 / 255.0, green: 92.0 / 255.0, blue: 192.0 / 255.0, alpha: 1.0).cgColor

        self.gl = CAGradientLayer()
        self.gl.colors = [colorTop, colorBottom]
        self.gl.locations = [0.0, 1.0]
    }
}

func Set_Card_View(Card: UIView){
    Card.layer.cornerRadius = Card.frame.size.height / 34
}


func Set_Login_Radius(Button: UIButton){
    Button.layer.cornerRadius = Button.frame.size.height / 10
}
func SaveData(country_id : String){
   // let country_id = txt_contry_id.text!
    UserDefaults.standard.set(country_id, forKey: CountryID)
}

func SetTextFieldLeftSide(imageName : String, txt_field: UITextField){
    txt_field.leftViewMode = .always
    let view = UIView(frame: CGRect(x : 15, y : 0, width : 18, height : 28))
    let imageView = UIImageView(frame: CGRect(x : 15, y : 0, width : 28, height : 18))
    view.addSubview(imageView)
    view.clipsToBounds = true
     imageView.contentMode = .scaleAspectFit
    let image = UIImage(named: imageName)
    imageView.image = image
    imageView.tintColor = UIColor.lightGray
    txt_field.leftView = imageView
    
}


func SetTextFieldRightSide(imageName : String, txt_field: UITextField){
    txt_field.rightViewMode = .always
    let view = UIView(frame: CGRect(x : 0, y : 0, width : 38, height : 28))
    let imageView = UIImageView(frame: CGRect(x : 15, y : 5, width : 10, height : 18))
    view.addSubview(imageView)
    view.clipsToBounds = true
    imageView.contentMode = .scaleAspectFit
    let image = UIImage(named: imageName)
    imageView.image = image
    imageView.tintColor = UIColor.lightGray
    txt_field.rightView = view
    
}


    func setLanguage(lang : String) -> Bundle{
    let path = Bundle.main.path(forResource: lang, ofType: "lproj")
    let bundle = Bundle.init(path: path!)! as Bundle
        return bundle
    }


extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
       // layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
//        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
//        layer.shouldRasterize = true
//        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
//        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//        layer.shouldRasterize = true
//        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}


    func SaveLoginKey(key : String){
        UserDefaults.standard.set(key, forKey: LoginKey)
    }

     

    func IsLoginKey() -> Bool{
        let value = UserDefaults.standard.value(forKey: LoginKey) as! String
        
        if value != ""{
            return true
        }else{
            return false
        }
//
//        if UserDefaults.standard.value(forKey: LoginKey) != nil{
//            return true
//        }else{
//            return false
//        }
        
    }



func SetViewOnHeader(table : UITableView, section : Int, num_Section : Int, expend_col: [Int]) -> UIView{
    let view : UIView = UIView(frame: CGRect(x : 0, y : 0, width : table.frame.size.width, height: table.frame.size.height / 12))
    let name : UILabel = UILabel(frame: CGRect(x : table.frame.size.width / 5.3, y : 0, width : table.frame.size.width / 1.8, height: table.frame.size.height / 12))
    name.text = "Abhishek"
    name.font = name.font.withSize(15)
    name.textAlignment = .left
    let originY = name.frame.size.height / 2.3
    let originX = table.frame.size.width / 1.2
    let expend_sideBar : UIButton = UIButton(frame: CGRect(x : originX, y : originY, width : 10, height : 10))
    let origImage = UIImage(named: Extend);
    let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
    expend_sideBar.setImage(tintedImage, for: .normal)
    expend_sideBar.tintColor = UIColor.darkGray
    expend_sideBar.addTarget(SideBar.self, action: #selector(SideBar.expend_sideBar_pressed(sender:)), for: .touchUpInside)
    expend_sideBar.tag = section
    for check_col in 1..<num_Section{
        if section == expend_col[check_col]{
            view.addSubview(expend_sideBar)
        }
    }
    view.addSubview(name)
    return view
}

func GetLatitudeFromData(Vehicals : Dictionary<String, Any>) -> Double{
    
    if let latitude = Vehicals["latitude"] as? String{
        
        let c_latitude = Double(latitude)
       
        return c_latitude!
    }
    else {
        return 0.0
    }
}

func GetDirectionFromData(Vehicals : Dictionary<String, Any>) -> String{
    //direction
    if let registration_no = Vehicals["direction"] as? String{
        
        return registration_no
    }
    else {
        return ""
    }
}

func GetLonitudeFromData(Vehicals : Dictionary<String, Any>) -> Double{
    
    if let latitude = Vehicals["longitude"] as? String{
        let c_latitude = Double(latitude)
        return c_latitude!
    }
    else {
        return 0.0
    }
    
  
}

func GetRegistrationNumber(Vehicals : Dictionary<String, Any>) -> String{
    
    if let registration_no = Vehicals["registration_no"] as? String{
        
        return registration_no
    }
    else {
        return ""
    }
    
}

func GetTotalWorkingHours(Vehicals : Dictionary<String, Any>) -> String{
    //total_working_hours
    if let registration_no = Vehicals["total_working_hours"] as? String{
        
        return registration_no
    }
    else {
        return ""
    }
}

func GetSumOfDistance(Vehicals : Dictionary<String, Any>) -> String{
    //sum_of_distance
    if let registration_no = Vehicals["sum_of_distance"] as? String{
        
        return registration_no
    }
    else {
        return ""
    }
    
}

func GetDeviceSerial(Vehicals : Dictionary<String, Any>) -> String{
    //device_serial
    if let registration_no = Vehicals["device_serial"] as? String{
        
        return registration_no
    }
    else {
        return ""
    }
}

func GetDeviceTag(Vehicals : Dictionary<String, Any>) -> String{
    //device_tag
    if let registration_no = Vehicals["device_tag"] as? String{
        
        return registration_no
    }
    else {
        return ""
    }
}

func GetVehicleStatus(Vehicals: Dictionary<String, Any>) -> String{
    if let registration_no = Vehicals["vehicle_status"] as? String{
        
        return registration_no
    }
    else {
        return ""
    }
}



func GetDeviceStatus(Vehicals : Dictionary<String, Any>) -> Int{
    
    if let registration_no = Vehicals["device_status"] as? Int{
        
        return registration_no
    }
    else {
        return 0
    }
    
}

func GetDeviceID(Vehicals : Dictionary<String, Any>) -> String{
    
    if let device_id = Vehicals["device_id"] as? String{
        
        return device_id
    }
    else {
        return ""
    }
}

func GetVoiceNumber(Vehicals : Dictionary<String, Any>) -> String{
    //voice_no
    if let device_id = Vehicals["voice_no"] as? String{
        
        return device_id
    }
    else {
        return ""
    }
}

func GetVehicleTypeName(Vehicals : Dictionary<String, Any>) -> String{
    //vehicle_type_name
    if let device_id = Vehicals["vehicle_type_name"] as? String{
        
        return device_id
    }
    else {
        return ""
    }
}

func GetVehicleTypeID(Vehicals : Dictionary<String, Any>) -> String{
  //vehicle_type_id
    if let device_id = Vehicals["vehicle_type_id"] as? String{
        
        return device_id
    }
    else {
        return ""
    }
    
}

func GetPlaceFromData(Vehicals : Dictionary<String, Any>) -> String{
    //place
    if let device_id = Vehicals["place"] as? String{
        
        return device_id
    }
    else {
        return ""
    }
}

func GetStartDateFromData(Vehicals : Dictionary<String, Any>) -> String{
    //start_date
    if let device_id = Vehicals["start_date"] as? String{
        
        return device_id
    }
    else {
        return ""
    }
}

func GetAlertTypeName(alert_child: Dictionary<String, Any>) -> String{

    if let device_id = alert_child["alert_type_name"] as? String{
        
        return device_id
    }
    else {
        return ""
    }
}

func GetAlertLocation(alert_child: Dictionary<String, Any>) -> String{
    
    if let device_id = alert_child["alert_location"] as? String{
        
        return device_id
    }
    else {
        return ""
    }
}



func GetAlertTime(alert_child: Dictionary<String, Any>) -> String{
    
    if let device_id = alert_child["alert_time"] as? String{
        
        return device_id
    }
    else {
        return ""
    }
}

func GetCommandType(alert_child: Dictionary<String, Any>) -> String{
    if let device_id = alert_child["command_type"] as? String{
        
        return device_id
    }
    else {
        return ""
    }
}

func GetCommandStatus(alert_child: Dictionary<String, Any>) -> String{
    if let device_id = alert_child["command_status"] as? String{
        
        return device_id
    }
    else {
        return ""
    }
}

func GetCommandResponse(alert_child: Dictionary<String, Any>) -> String{
    if let device_id = alert_child["command_response"] as? String{
        
        return device_id
    }
    else {
        return ""
    }
}

func GetNotificatioDate(notification: Dictionary<String, Any>, key: String) -> String{
    
    if let date = notification[key] as? String{
        
        return date
    }
    else {
        return "--"
    }
}

func GetSpeedFromData(Vehicals : Dictionary<String, Any>) -> String{
    if let device_id = Vehicals["speed"] as? String{
        
        return device_id
    }
    else {
        return ""
    }
}

func GetVehicleTypeId(Vehicals : Dictionary<String, Any>) -> String{
    if let device_id = Vehicals["vehicle_type_id"] as? String{
        
        return device_id
    }
    else {
        return ""
    }
}

func GetTaxValue(Vehicals : Dictionary<String, Any>) -> String{
    if let device_id = Vehicals["tax_value"] as? String{
        
        return device_id
    }
    else {
        return "0"
    }
}

func GetTaxName(Vehicals : Dictionary<String, Any>) -> String{
    if let device_id = Vehicals["tax_name"] as? String{
        
        return device_id
    }
    else {
        return ""
    }
}

func GetOverSpeed(alert_child: Dictionary<String, Any>) -> NSArray{
    
    if let device_id = alert_child["overspeed_data"] as? NSArray{
    
    return device_id
    }
    else {
        let ddd : NSArray = []
    return ddd
    }
    
}



func GetAppNotification(notification: Dictionary<String, Any>) -> String{
    if let app_notification = notification["app_notification"] as? String{
        
        return app_notification
    }
    else {
        return "0"
    }
    
}

func DidFunctionThings(buttonName: [UIButton], viewName: [UIView], buttonTagValue: Int, viewTagValue: Int){

    for val in 0...4{
        if buttonTagValue == buttonName[val].tag{
            buttonName[val].tintColor = UIColor.black
        }else{
            buttonName[val].tintColor = UIColor.lightGray
        }
        
        if viewTagValue == viewName[val].tag{
            viewName[val].isHidden = false
        }else{
            viewName[val].isHidden = true
        }
    }
}

func SetReportView(viewName: UIView){
   // viewName.layer.cornerRadius = viewName.frame.size.height / 29
    viewName.layer.borderColor = UIColor.lightGray.cgColor
    viewName.layer.borderWidth = 1
    viewName.layer.masksToBounds = true
}

func TodayToDate() -> String{
    let date = Date()
    let formetter = DateFormatter()
    formetter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let result = formetter.string(from: date)
    return result
}

func TodayFromDate() -> String{
    
    let date = Date()
    let formetter = DateFormatter()
    formetter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let result = formetter.string(from: date)
    
    let full_date_time = result.components(separatedBy: " ")
    let to_date = full_date_time[0]
    let fromdate = to_date + " 00:00:00"
    return fromdate
}

func GetToTime(time: String) -> String{
    let to_date_time = time
    let full_date_time = to_date_time.components(separatedBy: " ")
    
    let to_date = full_date_time[1]
    return to_date
}

func GetFromTime(time: String) -> String{
    let from_date_time = time
    let date_time = from_date_time.components(separatedBy: " ")
    
    let from_date = date_time[1]
    
    return from_date
    
}

func GetToDate(date : String) -> String{
    let to_date_time = date
    let full_date_time = to_date_time.components(separatedBy: " ")
    
    let to_date = full_date_time[0]
    return to_date
}

func GetFromDate(date: String) -> String{
    let from_date_time = date
    let date_time = from_date_time.components(separatedBy: " ")
    
    let from_date = date_time[0]
    
    return from_date
}

func YesterdayToDate() -> String{
    let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())
    let formetter = DateFormatter()
    formetter.dateFormat = "yyyy-MM-dd"
    let result = formetter.string(from: yesterday!)
    let todate = result + " 23:59:59"
    return todate

}

func YesterdayFromDate() -> String{
    let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())
    let formetter = DateFormatter()
    formetter.dateFormat = "yyyy-MM-dd"
    let result = formetter.string(from: yesterday!)
    let fromdate = result + " 00:00:00"
    return fromdate
}

extension String {
    var firstUppercased: String {
        return prefix(1).uppercased()  + dropFirst()
    }
    var firstCapitalized: String {
        return prefix(1).capitalized + dropFirst()
    }
}

func FixBoldBetweenText(firstString: String, boldFontName: String, lastString: String) -> NSAttributedString{
    
    let attrs1 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12, weight: .medium)]
    
    let start1 = NSAttributedString(string: boldFontName, attributes: attrs1)
    
    let string_start = NSMutableAttributedString()
    
    string_start.append(NSMutableAttributedString(string: firstString))
    string_start.append(start1)
    string_start.append(NSMutableAttributedString(string: lastString))
    
    return string_start
}

func Fix3BoldBetweenText(firstString: String, boldFontName: String, secondString: String, bold2FontName: String, lastString: String) -> NSAttributedString{
    
    let attrs1 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12, weight: .medium)]
    
    let start1 = NSAttributedString(string: boldFontName, attributes: attrs1)
    let start2 = NSAttributedString(string: bold2FontName, attributes: attrs1)
    
    let string_start = NSMutableAttributedString()
    
    string_start.append(NSMutableAttributedString(string: firstString))
    string_start.append(start1)
    string_start.append(NSMutableAttributedString(string: secondString))
    string_start.append(start2)
    string_start.append(NSMutableAttributedString(string: lastString))
    
    
    return string_start
}

func showToast(controller: UIViewController, message : String, seconds: Double) {
 
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.black
        alert.view.alpha = 0.6
        alert.view.layer.cornerRadius = 15
        
        controller.present(alert, animated: true)
        
    
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true)
        }
}

func showMessagePermissionOnToast(controller: UIViewController, mesage: String, second: Double){
    
    let test_alert = UIAlertController(title: nil, message: "Do You Want to Logout.", preferredStyle: .actionSheet)
    
    controller.present(test_alert, animated: true)
    
    
}


func GetAmountsWithTax(total_amount: Double, tax_value: String, tax_name: String) -> [String]{
    
    var last_amount_all_tax_include : Double = 0.0
    let taxV = tax_value.components(separatedBy: ",")
    
    for val in taxV{
        
        let tax = Double(val) ?? 0
        let payble_amount = total_amount * tax
        let last_payble_amount = Double(payble_amount) / 100
        last_amount_all_tax_include = last_amount_all_tax_include + last_payble_amount
    }
    
    let lbl_total_amount = "₹ " + String(last_amount_all_tax_include + total_amount)
    let last_amount_string = last_amount_all_tax_include.roundedtoPlaces(places: 3)
    let payble_amount_1 = "₹" + String(total_amount) + "+" + tax_name + "@"
    let payble_amount_2 = tax_value + "% (₹" + String(last_amount_string) + ")"
    let lbl_amount = "Amount Payble:   " + payble_amount_1 + payble_amount_2
    let total_payble_amount_with_tax = String(last_amount_all_tax_include + total_amount)
    return [lbl_total_amount, lbl_amount, total_payble_amount_with_tax]
    
}

extension UIViewController {
    var className: String {
        return NSStringFromClass(self.classForCoder).components(separatedBy: ".").last!;
    }
}



func SplitArray(into: Int, yourArray: Array<Any>) -> Array<Any>{
    var arrVal = Array<Any>()
    var path = 0
    for (_, _) in yourArray.enumerated(){
        if path < into{
            arrVal.append(yourArray[path] as! Dictionary<String, Any>)
            path = path + 1
        }
    }
    
    return arrVal
}

extension GMSMarker {
    func setIconSize(scaledToSize newSize: CGSize) {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        icon?.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        icon = newImage
    }
}

extension String {
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func roundedtoPlaces(places:Int) -> Double {
        let divisor = pow(100.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}


//2/12/2019
extension UIView {
    @discardableResult
    func applyGradient(colours: [UIColor]) -> CAGradientLayer {
        return self.applyGradient(colours: colours, locations: nil)
    }

    @discardableResult
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
}
