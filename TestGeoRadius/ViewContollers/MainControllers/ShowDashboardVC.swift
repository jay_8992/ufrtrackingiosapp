//
//  ShowDashboardVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 05/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit


class ShowDashboardVC: UIViewController {

    @IBOutlet weak var collection_items: UICollectionView!

    let item_name = ["Critical Alerts", "Today", "This Week", "Graph"]
    var item_count = [0, 0.0, 0.0, 0.0]
    private let numEntry = 20
    var count_status = [0, 0, 0, 0, 0]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collection_items.delegate = self
        collection_items.dataSource = self
        
        let nibName = UINib(nibName: "GraphCell", bundle:nil)
        
        collection_items.register(nibName, forCellWithReuseIdentifier: "GraphCell")
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
         NotificationCenter.default.addObserver(self, selector: #selector(GetTrackingData), name: Notification.Name("TrackingData"), object: nil)
    }
    
    
    @objc func GetTrackingData(notification: Notification){
        
        let c_data = notification.userInfo
        self.count_status.removeAll()
        var today : Double = 0
        var week : Double = 0
        let values = (c_data![VEHICALS] as! Array<Any>)
        
        let moving : Int = c_data!["moving_count"] as! Int
        let idle : Int =  c_data!["idling_count"] as! Int
        let stop : Int = c_data!["stopped_count"] as! Int
        let unreach : Int = c_data!["unreachable_count"] as! Int
        
        
        self.count_status.append(moving)
        self.count_status.append(idle)
        self.count_status.append(stop)
        self.count_status.append(unreach)
        
        
        let all_count =  moving + idle + stop + unreach
        self.count_status.append(all_count)

        
        for val_data in values{
            let val = val_data as! Dictionary<String, Any>
            let t_day = val["today_distance"] as! Double
            today = today + t_day
            let w_day = val["week_distance"] as! Double
            week = week + w_day
        }
        
        
        self.item_count[0] = Double(c_data!["alert_count"] as! Int).rounded()
        self.item_count[1] = today.rounded()
        self.item_count[2] = week.rounded()
      
        collection_items.reloadData()
    }
    
    
}

extension ShowDashboardVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return item_name.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collection_items.dequeueReusableCell(withReuseIdentifier: "DashboardItemCell", for: indexPath) as! DashboardItemCell
        let graph_cell = collection_items.dequeueReusableCell(withReuseIdentifier: "GraphCell", for: indexPath) as! GraphCell
        
       

        cell.view_item.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        cell.lbl_alert.text = item_name[indexPath.item]
        
        if indexPath.item > 2{
            graph_cell.values = self.count_status
            graph_cell.SetGraph()
            return graph_cell
        }else{
            if indexPath.item > 0{
                cell.lbl_count.text = String(item_count[indexPath.row]) + " km"
            }else{
                cell.lbl_count.text = String(item_count[indexPath.row])
            }
            
            
            cell.view_item.layer.cornerRadius = 8
            return cell
        }
        
        
       
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.item > 2{
           return CGSize(width: self.collection_items.frame.size.width, height: self.collection_items.frame.size.height / 2)
        }else{
            return CGSize(width: self.collection_items.frame.size.width, height: self.collection_items.frame.size.height / 4.2)
        }
        
    }
    
    
}
