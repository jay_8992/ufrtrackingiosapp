//
//  TripDetailVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 20/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class TripDetailVC: UIViewController {

    @IBOutlet weak var header_view: UIView!
    @IBOutlet weak var tbl_report: UITableView!
    @IBOutlet weak var lbl_error: UILabel!
    
    let alert_view = AlertView.instanceFromNib()
    var avg_speed = [String]()
    var distance = [String]()
    var date_time = [String]()
    var start = [String]()
    var end = [String]()
    var start_date = [String]()
    var end_date = [String]()
    var vehical_status = [String]()
    var urlString : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        header_view.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)

        let buttonTop = UIButton(frame: CGRect(x: self.view.frame.size.width - 70, y: self.view.frame.size.height - 100, width: 50, height: 50))
        buttonTop.backgroundColor = UIColor.darkGray
        buttonTop.layer.cornerRadius = buttonTop.frame.size.width / 2
        buttonTop.setImage(UIImage(named: "up"), for: .normal)
        buttonTop.addTarget(self, action: #selector(BottomToTop), for: .touchUpInside)
        self.view.addSubview(buttonTop)
       
        self.tbl_report.register(UINib(nibName: "TripDetailCell", bundle: nil), forCellReuseIdentifier: "TripDetailCell")

       CallDataFromServer()
        // Do any additional setup after loading the view.
    }
    
    @objc func BottomToTop(){
        let indexPath = IndexPath(row: 0, section: 0)
        self.tbl_report.scrollToRow(at: indexPath, at: .top, animated: true)
    }

    @IBAction func back_pressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func CallDataFromServer(){
        //alert_data_registration.removeAll()
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        self.view.addSubview(alert_view)
   
      
        let encodedString = urlString!.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: " ").inverted)
        CallTripReport(urlString: encodedString!, key_val: "trip_moving_data", completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                self.tbl_report.isHidden = false
                
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                  
                    self.avg_speed.append(val_data["speed"] as! String)
                   
                    self.start.append(val_data["start_location_full"] as! String)
                    self.end.append(val_data["end_location_full"] as! String)
                    self.start_date.append(val_data["start_date"] as! String)
                    self.end_date.append(val_data["end_date"] as! String)
                  
                }
                self.tbl_report.delegate = self
                self.tbl_report.dataSource = self
                self.tbl_report.reloadData()
            }else{
                if data != nil{
                    self.lbl_error.text = "Something went wrong."
                    self.tbl_report.isHidden = true
                }else{
                    showToast(controller: self, message: "Please check your Internet Connection.", seconds: 0.3)
                    self.tbl_report.isHidden = true
                }
                
             
            }
            
            if r_error != nil{
                self.lbl_error.text = r_error
                self.tbl_report.isHidden = true
            }
            self.alert_view.removeFromSuperview()
        })
    }
    

}
extension TripDetailVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_report.dequeueReusableCell(withIdentifier: "TripDetailCell") as! TripDetailCell
      
        cell.lbl_avg.text = avg_speed[indexPath.row] + " KMPH"
      
        
        cell.lbl_start.attributedText = FixBoldBetweenText(firstString: self.start[indexPath.row], boldFontName: " on ", lastString: self.start_date[indexPath.row])
        
        
        cell.lbl_end.attributedText = FixBoldBetweenText(firstString: self.end[indexPath.row], boldFontName: " on ", lastString: self.end_date[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_report.frame.size.height / 2.5
    }
    
}
