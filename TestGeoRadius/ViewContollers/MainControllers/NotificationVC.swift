//
//  NotificationVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 11/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit


class NotificationVC: UIViewController {

    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var tbl_notification: UITableView!
    
    @IBOutlet weak var lbl_error: UILabel!
    let alert_view = AlertView.instanceFromNib()
    var message = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tbl_notification.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)

       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        CallDataFromServer()
    }
    
    @IBAction func back_pressed(_ sender: Any) {
        
        let isNotifiaction = UserDefaults.standard.value(forKey: "IsNotification") as! Bool
            
              if isNotifiaction{
                  self.dismiss(animated: true, completion: nil)
              }else{
                  
                   self.navigationController!.popViewController(animated: true)
              }
       
       // self.navigationController!.popViewController(animated: true)
    }
  
    
    func CallDataFromServer(){
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.tbl_notification.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        
        self.view.addSubview(alert_view)
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
      
         let device_token = UserDefaults.standard.value(forKey: "DEVICE_TOKEN") as! String
        let urlString = domain_name + "/report_alert_result.php?action=user_specific_app_alert&user_name=" + user_name + "&hash_key=" + hash_key + "&user_app_id=" + device_token
        
     //   print("dlkdlk \(urlString)")
        
        CallNotificatioSettingDataFromServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                   self.message.append(val_data["message"] as! String)
                    self.tbl_notification.isHidden = false
                    self.tbl_notification.delegate = self
                    self.tbl_notification.dataSource = self
                    self.tbl_notification.reloadData()
                    self.lbl_error.isHidden = true
                }
                
            }else{
                //showToast(controller: self, message : "Something went wrong.", seconds: 0.2)
                self.tbl_notification.isHidden = true
                self.lbl_error.isHidden = false
                print("ERROR FOUND")
            }
            if r_error != nil{
                //showToast(controller: self, message : "Something went wrong.", seconds: 0.2)
                self.tbl_notification.isHidden = true
                self.lbl_error.isHidden = false
            }
            
            self.alert_view.removeFromSuperview()
        })
        
    }
}


extension NotificationVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return message.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_notification.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        cell.lbl_message.text = message[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_notification.frame.size.height / 6
    }
    
}
