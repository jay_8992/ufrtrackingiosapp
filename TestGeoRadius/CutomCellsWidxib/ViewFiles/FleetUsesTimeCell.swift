//
//  FleetUsesTimeCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 18/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class FleetUsesTimeCell: UITableViewCell {

    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var lbl_stopped: UILabel!
    @IBOutlet weak var lbl_moving: UILabel!
    @IBOutlet weak var lbl_idle: UILabel!
    @IBOutlet weak var view_stopped: UIView!
    @IBOutlet weak var view_moving: UIView!
    @IBOutlet weak var view_idle: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        view_idle.backgroundColor = Idle_Color
        view_moving.backgroundColor = Moving_Color
        view_stopped.backgroundColor = Stopped_Color
        // Configure the view for the selected state
    }
    
}
